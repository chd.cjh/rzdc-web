var ziroomIndex = {};

ziroomIndex.init = function () {

    ziroomIndex.loadFoucsImg('foucsSlideList');

    var scale = 600 / 1920;
    $(window).resize(function () {

        var w = $(window).width();

        if (w < 1200) {
            w = 1200;
        }

        var h = w * scale;

        $('#foucsSlideList,#foucsSlideList li,#foucsSlideList li img,.foucsSlide').css({
            'width': w + 'px'
        });
        $('#foucsSlideList,#foucsSlideList li,#foucsSlideList li img,.foucsSlide').css({
            'height': h + 'px'
        });
    });
    var w = $(window).width();

    if (w < 1200) {
        w = 1200;
    }

    var h = w * scale;

    $('#foucsSlideList,#foucsSlideList li,#foucsSlideList li img,.foucsSlide').css({
        'width': w + 'px'
    });
    $('#foucsSlideList,#foucsSlideList li,#foucsSlideList li img,.foucsSlide').css({
        'height': h + 'px'
    });
};


ziroomIndex.loadFoucsImg = function (obj) {
    var ID = $('#' + obj);
    var aImg = ID.find('img');
    var tmp = [];
    var iLoaded = 0;
    var n = 0;

    for (var i = 0; i < aImg.size(); i++) {
        tmp.push(aImg.eq(i).attr('_src'));
    }

    aImg.eq(0).attr('src', tmp[0]);
    var timer = setInterval(function () {
        n++;
        if (n > aImg.size() - 1) {
            clearInterval(timer);
        }
        aImg.eq(n).attr('src', tmp[n]);
    }, 500);

}


$(function () {
    ziroomIndex.init();
    jQuery(".foucsSlide").slide({
        mainCell: ".bd ul",
        effect: "fold",
        autoPlay: true,
        interTime: 5000
    });


    $(".categorys dl").each(function () {
        var a = $(this).find("dd").find(".detail");
        a.each(function () {
            $(this).find("a").size() < 2 && $(this).hide()
        })
    });

    $('#searchType span').click(function () {
        $('#searchTxt').html($(this).html() + '<b></b>');
        $('#searchType').hide();
        $('#searchTxt').attr('data-val', $(this).attr('data-val'));
    });

    $('#searchList').hover(function () {
        $('#searchType').show();
    }, function () {
        $('#searchType').hide();
    });
});
