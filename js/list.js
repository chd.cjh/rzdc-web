$(function () {
    var listUtils = {};

    listUtils.init = function () {
        listUtils.filterShow();
    };

    listUtils.filterShow = function () {
        var filterTimer = null;
        $('.filterList li').hover(function () {

            var _top = $(this).offset().top;
            var _tops = $(this).parents().offset().top;
            $(this).find('.tag').css({
                'z-index': 5
            });
            var topEnd = _top - _tops + 32;

            $(this).find('.con').css({
                'top': topEnd + 'px',
                'z-index': 4
            }).show();
            $(this).addClass('active');
        }, function () {
            $(this).find('.con').css({
                'z-index': 1
            }).hide();
            $(this).find('.tag').css({
                'z-index': 2
            });
            $(this).removeClass('active');
        });
    };

    listUtils.init();
});