! function (a, b) {
    "use strict";
    var c, d, e = {
            getPath: function () {
                var a = document.scripts,
                    b = a[a.length - 1],
                    c = b.src;
                if (!b.getAttribute("merge")) return c.substring(0, c.lastIndexOf("/") + 1);
            }(),
            enter: function (a) {
                13 === a.keyCode && a.preventDefault();
            },
            config: {},
            end: {},
            btn: ["&#x786E;&#x5B9A;", "&#x53D6;&#x6D88;"],
            type: ["dialog", "page", "iframe", "loading", "tips"]
        },
        f = {
            v: "2.4",
            ie6: !!a.ActiveXObject && !a.XMLHttpRequest,
            index: 0,
            path: e.getPath,
            config: function (a, b) {
                var d = 0;
                return a = a || {}, f.cache = e.config = c.extend(e.config, a), f.path = e.config.path || f.path,
                    "string" == typeof a.extend && (a.extend = [a.extend]), f.use("http://static8.ziroom.com/phoenix/pc/css/layerskin/layer.css", a.extend && a.extend.length > 0 ? function g() {
                        var c = a.extend;
                        f.use(c[c[d] ? d : d - 1], d < c.length ? function () {
                            return ++d, g;
                        }() : b);
                    }() : b), this;
            },
            use: function (a, b, d) {
                var e = c("head")[0],
                    a = a.replace(/\s/g, ""),
                    g = /\.css$/.test(a),
                    h = document.createElement(g ? "link" : "script"),
                    i = "layui_layer_" + a.replace(/\.|\//g, "");
                return f.path ? (g && (h.rel = "stylesheet"), h[g ? "href" : "src"] = /^http:\/\//.test(a) ? a : f.path + a,
                    h.id = i, c("#" + i)[0] || e.appendChild(h),
                    function j() {
                        (g ? 1989 === parseInt(c("#" + i).css("width")) : f[d || i]) ? function () {
                            b && b();
                            try {
                                g || e.removeChild(h);
                            } catch (a) {}
                        }() : setTimeout(j, 100);
                    }(), this) : void 0;
            },
            ready: function (a, b) {
                var d = "function" == typeof a;
                return d && (b = a), f.config(c.extend(e.config, function () {
                    return d ? {} : {
                        path: a
                    };
                }()), b), this;
            },
            alert: function (a, b, d) {
                var e = "function" == typeof b;
                return e && (d = b), f.open(c.extend({
                    content: a,
                    yes: d
                }, e ? {} : b));
            },
            confirm: function (a, b, d, g) {
                var h = "function" == typeof b;
                return h && (g = d, d = b), f.open(c.extend({
                    content: a,
                    btn: e.btn,
                    yes: d,
                    btn2: g
                }, h ? {} : b));
            },
            msg: function (a, d, g) {
                var i = "function" == typeof d,
                    j = e.config.skin,
                    k = (j ? j + " " + j + "-msg" : "") || "layui-layer-msg",
                    l = h.anim.length - 1;
                return i && (g = d), f.open(c.extend({
                    content: a,
                    time: 3e3,
                    shade: !1,
                    skin: k,
                    title: !1,
                    closeBtn: !1,
                    btn: !1,
                    end: g
                }, i && !e.config.skin ? {
                    skin: k + " layui-layer-hui",
                    shift: l
                } : function () {
                    return d = d || {}, (-1 === d.icon || d.icon === b && !e.config.skin) && (d.skin = k + " " + (d.skin || "layui-layer-hui")),
                        d;
                }()));
            },
            load: function (a, b) {
                return f.open(c.extend({
                    type: 3,
                    icon: a || 0,
                    shade: .01
                }, b));
            },
            tips: function (a, b, d) {
                return f.open(c.extend({
                    type: 4,
                    content: [a, b],
                    closeBtn: !1,
                    time: 3e3,
                    shade: !1,
                    fix: !1,
                    maxWidth: 210
                }, d));
            }
        },
        g = function (a) {
            var b = this;
            b.index = ++f.index, b.config = c.extend({}, b.config, e.config, a), b.creat();
        };
    g.pt = g.prototype;
    var h = ["layui-layer", ".layui-layer-title", ".layui-layer-main", ".layui-layer-dialog", "layui-layer-iframe", "layui-layer-content", "layui-layer-btn", "layui-layer-close"];
    h.anim = ["layer-anim", "layer-anim-01", "layer-anim-02", "layer-anim-03", "layer-anim-04", "layer-anim-05", "layer-anim-06"],
        g.pt.config = {
            type: 0,
            shade: .3,
            fix: !0,
            move: h[1],
            title: "&#x4FE1;&#x606F;",
            offset: "auto",
            area: "auto",
            closeBtn: 1,
            time: 0,
            zIndex: 19891014,
            maxWidth: 360,
            shift: 0,
            icon: -1,
            scrollbar: !0,
            tips: 2
        }, g.pt.vessel = function (a, b) {
            var c = this,
                d = c.index,
                f = c.config,
                g = f.zIndex + d,
                i = "object" == typeof f.title,
                j = f.maxmin && (1 === f.type || 2 === f.type),
                k = f.title ? '<div class="layui-layer-title" style="' + (i ? f.title[1] : "") + '">' + (i ? f.title[0] : f.title) + "</div>" : "";
            return f.zIndex = g, b([f.shade ? '<div class="layui-layer-shade" id="layui-layer-shade' + d + '" times="' + d + '" style="' + ("z-index:" + (g - 1) + "; background-color:" + (f.shade[1] || "#000") + "; opacity:" + (f.shade[0] || f.shade) + "; filter:alpha(opacity=" + (100 * f.shade[0] || 100 * f.shade) + ");") + '"></div>' : "", '<div class="' + h[0] + (" layui-layer-" + e.type[f.type]) + (0 != f.type && 2 != f.type || f.shade ? "" : " layui-layer-border") + " " + (f.skin || "") + '" id="' + h[0] + d + '" type="' + e.type[f.type] + '" times="' + d + '" showtime="' + f.time + '" conType="' + (a ? "object" : "string") + '" style="z-index: ' + g + "; width:" + f.area[0] + ";height:" + f.area[1] + (f.fix ? "" : ";position:absolute;") + '">' + (a && 2 != f.type ? "" : k) + '<div id="' + (f.id || "") + '" class="layui-layer-content' + (0 == f.type && -1 !== f.icon ? " layui-layer-padding" : "") + (3 == f.type ? " layui-layer-loading" + f.icon : "") + '">' + (0 == f.type && -1 !== f.icon ? '<i class="layui-layer-ico layui-layer-ico' + f.icon + '"></i>' : "") + (1 == f.type && a ? "" : f.content || "") + '</div><span class="layui-layer-setwin">' + function () {
                var a = j ? '<a class="layui-layer-min" href="javascript:;"><cite></cite></a><a class="layui-layer-ico layui-layer-max" href="javascript:;"></a>' : "";
                return f.closeBtn && (a += '<a class="layui-layer-ico ' + h[7] + " " + h[7] + (f.title ? f.closeBtn : 4 == f.type ? "1" : "2") + '" href="javascript:;"></a>'),
                    a;
            }() + "</span>" + (f.btn ? function () {
                var a = "";
                "string" == typeof f.btn && (f.btn = [f.btn]);
                for (var b = 0, c = f.btn.length; c > b; b++) a += '<a class="' + h[6] + b + '">' + f.btn[b] + "</a>";
                return '<div class="' + h[6] + '">' + a + "</div>";
            }() : "") + "</div>"], k), c;
        }, g.pt.creat = function () {
            var a = this,
                b = a.config,
                g = a.index,
                i = b.content,
                j = "object" == typeof i;
            if (!c("#" + b.id)[0]) {
                switch ("string" == typeof b.area && (b.area = "auto" === b.area ? ["", ""] : [b.area, ""]),
                    b.type) {
                    case 0:
                        b.btn = "btn" in b ? b.btn : e.btn[0], f.closeAll("dialog");
                        break;

                    case 2:
                        var i = b.content = j ? b.content : [b.content || "http://layer.layui.com", "auto"];
                        b.content = '<iframe scrolling="' + (b.content[1] || "auto") + '" allowtransparency="true" id="' + h[4] + g + '" name="' + h[4] + g + '" onload="this.className=\'\';" class="layui-layer-load" frameborder="0" src="' + b.content[0] + '"></iframe>';
                        break;

                    case 3:
                        b.title = !1, b.closeBtn = !1, -1 === b.icon && 0 === b.icon, f.closeAll("loading");
                        break;

                    case 4:
                        j || (b.content = [b.content, "body"]), b.follow = b.content[1], b.content = b.content[0] + '<i class="layui-layer-TipsG"></i>',
                            b.title = !1, b.tips = "object" == typeof b.tips ? b.tips : [b.tips, !0], b.tipsMore || f.closeAll("tips");
                }
                a.vessel(j, function (d, e) {
                        c("body").append(d[0]), j ? function () {
                            2 == b.type || 4 == b.type ? function () {
                                c("body").append(d[1]);
                            }() : function () {
                                i.parents("." + h[0])[0] || (i.show().addClass("layui-layer-wrap").wrap(d[1]), c("#" + h[0] + g).find("." + h[5]).before(e));
                            }();
                        }() : c("body").append(d[1]), a.layero = c("#" + h[0] + g), b.scrollbar || h.html.css("overflow", "hidden").attr("layer-full", g);
                    }).auto(g), 2 == b.type && f.ie6 && a.layero.find("iframe").attr("src", i[0]), c(document).off("keydown", e.enter).on("keydown", e.enter),
                    a.layero.on("keydown", function (a) {
                        c(document).off("keydown", e.enter);
                    }), 4 == b.type ? a.tips() : a.offset(), b.fix && d.on("resize", function () {
                        a.offset(), (/^\d+%$/.test(b.area[0]) || /^\d+%$/.test(b.area[1])) && a.auto(g),
                            4 == b.type && a.tips();
                    }), b.time <= 0 || setTimeout(function () {
                        f.close(a.index);
                    }, b.time), a.move().callback(), h.anim[b.shift] && a.layero.addClass(h.anim[b.shift]);
            }
        }, g.pt.auto = function (a) {
            function b(a) {
                a = g.find(a), a.height(i[1] - j - k - 2 * (0 | parseFloat(a.css("padding"))));
            }
            var e = this,
                f = e.config,
                g = c("#" + h[0] + a);
            "" === f.area[0] && f.maxWidth > 0 && (/MSIE 7/.test(navigator.userAgent) && f.btn && g.width(g.innerWidth()),
                g.outerWidth() > f.maxWidth && g.width(f.maxWidth));
            var i = [g.innerWidth(), g.innerHeight()],
                j = g.find(h[1]).outerHeight() || 0,
                k = g.find("." + h[6]).outerHeight() || 0;
            switch (f.type) {
                case 2:
                    b("iframe");
                    break;

                default:
                    "" === f.area[1] ? f.fix && i[1] >= d.height() && (i[1] = d.height(), b("." + h[5])) : b("." + h[5]);
            }
            return e;
        }, g.pt.offset = function () {
            var a = this,
                b = a.config,
                c = a.layero,
                e = [c.outerWidth(), c.outerHeight()],
                f = "object" == typeof b.offset;
            a.offsetTop = (d.height() - e[1]) / 2, a.offsetLeft = (d.width() - e[0]) / 2, f ? (a.offsetTop = b.offset[0],
                    a.offsetLeft = b.offset[1] || a.offsetLeft) : "auto" !== b.offset && (a.offsetTop = b.offset,
                    "rb" === b.offset && (a.offsetTop = d.height() - e[1], a.offsetLeft = d.width() - e[0])),
                b.fix || (a.offsetTop = /%$/.test(a.offsetTop) ? d.height() * parseFloat(a.offsetTop) / 100 : parseFloat(a.offsetTop),
                    a.offsetLeft = /%$/.test(a.offsetLeft) ? d.width() * parseFloat(a.offsetLeft) / 100 : parseFloat(a.offsetLeft),
                    a.offsetTop += d.scrollTop(), a.offsetLeft += d.scrollLeft()), c.css({
                    top: a.offsetTop,
                    left: a.offsetLeft
                });
        }, g.pt.tips = function () {
            var a = this,
                b = a.config,
                e = a.layero,
                f = [e.outerWidth(), e.outerHeight()],
                g = c(b.follow);
            g[0] || (g = c("body"));
            var i = {
                    width: g.outerWidth(),
                    height: g.outerHeight(),
                    top: g.offset().top,
                    left: g.offset().left
                },
                j = e.find(".layui-layer-TipsG"),
                k = b.tips[0];
            b.tips[1] || j.remove(), i.autoLeft = function () {
                    i.left + f[0] - d.width() > 0 ? (i.tipLeft = i.left + i.width - f[0], j.css({
                        right: 12,
                        left: "auto"
                    })) : i.tipLeft = i.left;
                }, i.where = [function () {
                    i.autoLeft(), i.tipTop = i.top - f[1] - 10, j.removeClass("layui-layer-TipsB").addClass("layui-layer-TipsT").css("border-right-color", b.tips[1]);
                }, function () {
                    i.tipLeft = i.left + i.width + 10, i.tipTop = i.top, j.removeClass("layui-layer-TipsL").addClass("layui-layer-TipsR").css("border-bottom-color", b.tips[1]);
                }, function () {
                    i.autoLeft(), i.tipTop = i.top + i.height + 10, j.removeClass("layui-layer-TipsT").addClass("layui-layer-TipsB").css("border-right-color", b.tips[1]);
                }, function () {
                    i.tipLeft = i.left - f[0] - 10, i.tipTop = i.top, j.removeClass("layui-layer-TipsR").addClass("layui-layer-TipsL").css("border-bottom-color", b.tips[1]);
                }], i.where[k - 1](), 1 === k ? i.top - (d.scrollTop() + f[1] + 16) < 0 && i.where[2]() : 2 === k ? d.width() - (i.left + i.width + f[0] + 16) > 0 || i.where[3]() : 3 === k ? i.top - d.scrollTop() + i.height + f[1] + 16 - d.height() > 0 && i.where[0]() : 4 === k && f[0] + 16 - i.left > 0 && i.where[1](),
                e.find("." + h[5]).css({
                    "background-color": b.tips[1],
                    "padding-right": b.closeBtn ? "30px" : ""
                }), e.css({
                    left: i.tipLeft - (b.fix ? d.scrollLeft() : 0),
                    top: i.tipTop - (b.fix ? d.scrollTop() : 0)
                });
        }, g.pt.move = function () {
            var a = this,
                b = a.config,
                e = {
                    setY: 0,
                    moveLayer: function () {
                        var a = e.layero,
                            b = parseInt(a.css("margin-left")),
                            c = parseInt(e.move.css("left"));
                        0 === b || (c -= b), "fixed" !== a.css("position") && (c -= a.parent().offset().left,
                            e.setY = 0), a.css({
                            left: c,
                            top: parseInt(e.move.css("top")) - e.setY
                        });
                    }
                },
                f = a.layero.find(b.move);
            return b.move && f.attr("move", "ok"), f.css({
                cursor: b.move ? "move" : "auto"
            }), c(b.move).on("mousedown", function (a) {
                if (a.preventDefault(), "ok" === c(this).attr("move")) {
                    e.ismove = !0, e.layero = c(this).parents("." + h[0]);
                    var f = e.layero.offset().left,
                        g = e.layero.offset().top,
                        i = e.layero.outerWidth() - 6,
                        j = e.layero.outerHeight() - 6;
                    c("#layui-layer-moves")[0] || c("body").append('<div id="layui-layer-moves" class="layui-layer-moves" style="left:' + f + "px; top:" + g + "px; width:" + i + "px; height:" + j + 'px; z-index:2147483584"></div>'),
                        e.move = c("#layui-layer-moves"), b.moveType && e.move.css({
                            visibility: "hidden"
                        }), e.moveX = a.pageX - e.move.position().left, e.moveY = a.pageY - e.move.position().top,
                        "fixed" !== e.layero.css("position") || (e.setY = d.scrollTop());
                }
            }), c(document).mousemove(function (a) {
                if (e.ismove) {
                    var c = a.pageX - e.moveX,
                        f = a.pageY - e.moveY;
                    if (a.preventDefault(), !b.moveOut) {
                        e.setY = d.scrollTop();
                        var g = d.width() - e.move.outerWidth(),
                            h = e.setY;
                        0 > c && (c = 0), c > g && (c = g), h > f && (f = h), f > d.height() - e.move.outerHeight() + e.setY && (f = d.height() - e.move.outerHeight() + e.setY);
                    }
                    e.move.css({
                        left: c,
                        top: f
                    }), b.moveType && e.moveLayer(), c = f = g = h = null;
                }
            }).mouseup(function () {
                try {
                    e.ismove && (e.moveLayer(), e.move.remove(), b.moveEnd && b.moveEnd()), e.ismove = !1;
                } catch (a) {
                    e.ismove = !1;
                }
            }), a;
        }, g.pt.callback = function () {
            function a() {
                var a = g.cancel && g.cancel(b.index, d);
                a === !1 || f.close(b.index);
            }
            var b = this,
                d = b.layero,
                g = b.config;
            b.openLayer(), g.success && (2 == g.type ? d.find("iframe").on("load", function () {
                g.success(d, b.index);
            }) : g.success(d, b.index)), f.ie6 && b.IE6(d), d.find("." + h[6]).children("a").on("click", function () {
                var a = c(this).index();
                if (0 === a) g.yes ? g.yes(b.index, d) : g.btn1 ? g.btn1(b.index, d) : f.close(b.index);
                else {
                    var e = g["btn" + (a + 1)] && g["btn" + (a + 1)](b.index, d);
                    e === !1 || f.close(b.index);
                }
            }), d.find("." + h[7]).on("click", a), g.shadeClose && c("#layui-layer-shade" + b.index).on("click", function () {
                f.close(b.index);
            }), d.find(".layui-layer-min").on("click", function () {
                var a = g.min && g.min(d);
                a === !1 || f.min(b.index, g);
            }), d.find(".layui-layer-max").on("click", function () {
                c(this).hasClass("layui-layer-maxmin") ? (f.restore(b.index), g.restore && g.restore(d)) : (f.full(b.index, g),
                    setTimeout(function () {
                        g.full && g.full(d);
                    }, 100));
            }), g.end && (e.end[b.index] = g.end);
        }, e.reselect = function () {
            c.each(c("select"), function (a, b) {
                var d = c(this);
                d.parents("." + h[0])[0] || 1 == d.attr("layer") && c("." + h[0]).length < 1 && d.removeAttr("layer").show(),
                    d = null;
            });
        }, g.pt.IE6 = function (a) {
            function b() {
                a.css({
                    top: f + (e.config.fix ? d.scrollTop() : 0)
                });
            }
            var e = this,
                f = a.offset().top;
            b(), d.scroll(b), c("select").each(function (a, b) {
                var d = c(this);
                d.parents("." + h[0])[0] || "none" === d.css("display") || d.attr({
                    layer: "1"
                }).hide(), d = null;
            });
        }, g.pt.openLayer = function () {
            var a = this;
            f.zIndex = a.config.zIndex, f.setTop = function (a) {
                var b = function () {
                    f.zIndex++, a.css("z-index", f.zIndex + 1);
                };
                return f.zIndex = parseInt(a[0].style.zIndex), a.on("mousedown", b), f.zIndex;
            };
        }, e.record = function (a) {
            var b = [a.width(), a.height(), a.position().top, a.position().left + parseFloat(a.css("margin-left"))];
            a.find(".layui-layer-max").addClass("layui-layer-maxmin"), a.attr({
                area: b
            });
        }, e.rescollbar = function (a) {
            h.html.attr("layer-full") == a && (h.html[0].style.removeProperty ? h.html[0].style.removeProperty("overflow") : h.html[0].style.removeAttribute("overflow"),
                h.html.removeAttr("layer-full"));
        }, a.layer = f, f.getChildFrame = function (a, b) {
            return b = b || c("." + h[4]).attr("times"), c("#" + h[0] + b).find("iframe").contents().find(a);
        }, f.getFrameIndex = function (a) {
            return c("#" + a).parents("." + h[4]).attr("times");
        }, f.iframeAuto = function (a) {
            if (a) {
                var b = f.getChildFrame("html", a).outerHeight(),
                    d = c("#" + h[0] + a),
                    e = d.find(h[1]).outerHeight() || 0,
                    g = d.find("." + h[6]).outerHeight() || 0;
                d.css({
                    height: b + e + g
                }), d.find("iframe").css({
                    height: b
                });
            }
        }, f.iframeSrc = function (a, b) {
            c("#" + h[0] + a).find("iframe").attr("src", b);
        }, f.style = function (a, b) {
            var d = c("#" + h[0] + a),
                f = d.attr("type"),
                g = d.find(h[1]).outerHeight() || 0,
                i = d.find("." + h[6]).outerHeight() || 0;
            (f === e.type[1] || f === e.type[2]) && (d.css(b), f === e.type[2] && d.find("iframe").css({
                height: parseFloat(b.height) - g - i
            }));
        }, f.min = function (a, b) {
            var d = c("#" + h[0] + a),
                g = d.find(h[1]).outerHeight() || 0;
            e.record(d), f.style(a, {
                    width: 180,
                    height: g,
                    overflow: "hidden"
                }), d.find(".layui-layer-min").hide(), "page" === d.attr("type") && d.find(h[4]).hide(),
                e.rescollbar(a);
        }, f.restore = function (a) {
            var b = c("#" + h[0] + a),
                d = b.attr("area").split(",");
            b.attr("type");
            f.style(a, {
                    width: parseFloat(d[0]),
                    height: parseFloat(d[1]),
                    top: parseFloat(d[2]),
                    left: parseFloat(d[3]),
                    overflow: "visible"
                }), b.find(".layui-layer-max").removeClass("layui-layer-maxmin"), b.find(".layui-layer-min").show(),
                "page" === b.attr("type") && b.find(h[4]).show(), e.rescollbar(a);
        }, f.full = function (a) {
            var b, g = c("#" + h[0] + a);
            e.record(g), h.html.attr("layer-full") || h.html.css("overflow", "hidden").attr("layer-full", a),
                clearTimeout(b), b = setTimeout(function () {
                    var b = "fixed" === g.css("position");
                    f.style(a, {
                        top: b ? 0 : d.scrollTop(),
                        left: b ? 0 : d.scrollLeft(),
                        width: d.width(),
                        height: d.height()
                    }), g.find(".layui-layer-min").hide();
                }, 100);
        }, f.title = function (a, b) {
            var d = c("#" + h[0] + (b || f.index)).find(h[1]);
            d.html(a);
        }, f.close = function (a) {
            var b = c("#" + h[0] + a),
                d = b.attr("type");
            if (b[0]) {
                if (d === e.type[1] && "object" === b.attr("conType")) {
                    b.children(":not(." + h[5] + ")").remove();
                    for (var g = 0; 2 > g; g++) b.find(".layui-layer-wrap").unwrap().hide();
                } else {
                    if (d === e.type[2]) try {
                        var i = c("#" + h[4] + a)[0];
                        i.contentWindow.document.write(""), i.contentWindow.close(), b.find("." + h[5])[0].removeChild(i);
                    } catch (j) {}
                    b[0].innerHTML = "", b.remove();
                }
                c("#layui-layer-moves, #layui-layer-shade" + a).remove(), f.ie6 && e.reselect(),
                    e.rescollbar(a), c(document).off("keydown", e.enter), "function" == typeof e.end[a] && e.end[a](),
                    delete e.end[a];
            }
        }, f.closeAll = function (a) {
            c.each(c("." + h[0]), function () {
                var b = c(this),
                    d = a ? b.attr("type") === a : 1;
                d && f.close(b.attr("times")), d = null;
            });
        };
    var i = f.cache || {},
        j = function (a) {
            return i.skin ? " " + i.skin + " " + i.skin + "-" + a : "";
        };
    f.prompt = function (a, b) {
        a = a || {}, "function" == typeof a && (b = a);
        var d, e = 2 == a.formType ? '<textarea class="layui-layer-input">' + (a.value || "") + "</textarea>" : function () {
            return '<input type="' + (1 == a.formType ? "password" : "text") + '" class="layui-layer-input" value="' + (a.value || "") + '">';
        }();
        return f.open(c.extend({
            btn: ["&#x786E;&#x5B9A;", "&#x53D6;&#x6D88;"],
            content: e,
            skin: "layui-layer-prompt" + j("prompt"),
            success: function (a) {
                d = a.find(".layui-layer-input"), d.focus();
            },
            yes: function (c) {
                var e = d.val();
                "" === e ? d.focus() : e.length > (a.maxlength || 500) ? f.tips("&#x6700;&#x591A;&#x8F93;&#x5165;" + (a.maxlength || 500) + "&#x4E2A;&#x5B57;&#x6570;", d, {
                    tips: 1
                }) : b && b(e, c, d);
            }
        }, a));
    }, f.tab = function (a) {
        a = a || {};
        var b = a.tab || {};
        return f.open(c.extend({
            type: 1,
            skin: "layui-layer-tab" + j("tab"),
            title: function () {
                var a = b.length,
                    c = 1,
                    d = "";
                if (a > 0)
                    for (d = '<span class="layui-layer-tabnow">' + b[0].title + "</span>"; a > c; c++) d += "<span>" + b[c].title + "</span>";
                return d;
            }(),
            content: '<ul class="layui-layer-tabmain">' + function () {
                var a = b.length,
                    c = 1,
                    d = "";
                if (a > 0)
                    for (d = '<li class="layui-layer-tabli xubox_tab_layer">' + (b[0].content || "no content") + "</li>"; a > c; c++) d += '<li class="layui-layer-tabli">' + (b[c].content || "no  content") + "</li>";
                return d;
            }() + "</ul>",
            success: function (b) {
                var d = b.find(".layui-layer-title").children(),
                    e = b.find(".layui-layer-tabmain").children();
                d.on("mousedown", function (b) {
                    b.stopPropagation ? b.stopPropagation() : b.cancelBubble = !0;
                    var d = c(this),
                        f = d.index();
                    d.addClass("layui-layer-tabnow").siblings().removeClass("layui-layer-tabnow"), e.eq(f).show().siblings().hide(),
                        "function" == typeof a.change && a.change(f);
                });
            }
        }, a));
    }, f.photos = function (b, d, e) {
        function g(a, b, c) {
            var d = new Image();
            return d.src = a, d.complete ? b(d) : (d.onload = function () {
                d.onload = null, b(d);
            }, void(d.onerror = function (a) {
                d.onerror = null, c(a);
            }));
        }
        var h = {};
        if (b = b || {}, b.photos) {
            var i = b.photos.constructor === Object,
                k = i ? b.photos : {},
                l = k.data || [],
                m = k.start || 0;
            if (h.imgIndex = (0 | m) + 1, b.img = b.img || "img", i) {
                if (0 === l.length) return f.msg("&#x6CA1;&#x6709;&#x56FE;&#x7247;");
            } else {
                var n = c(b.photos),
                    o = function () {
                        l = [], n.find(b.img).each(function (a) {
                            var b = c(this);
                            b.attr("layer-index", a), l.push({
                                alt: b.attr("alt"),
                                pid: b.attr("layer-pid"),
                                src: b.attr("layer-src") || b.attr("src"),
                                thumb: b.attr("src")
                            });
                        });
                    };
                if (o(), 0 === l.length) return;
                if (d || n.on("click", b.img, function () {
                        var a = c(this),
                            d = a.attr("layer-index");
                        f.photos(c.extend(b, {
                            photos: {
                                start: d,
                                data: l,
                                tab: b.tab
                            },
                            full: b.full
                        }), !0), o();
                    }), !d) return;
            }
            h.imgprev = function (a) {
                h.imgIndex--, h.imgIndex < 1 && (h.imgIndex = l.length), h.tabimg(a);
            }, h.imgnext = function (a, b) {
                h.imgIndex++, h.imgIndex > l.length && (h.imgIndex = 1, b) || h.tabimg(a);
            }, h.keyup = function (a) {
                if (!h.end) {
                    var b = a.keyCode;
                    a.preventDefault(), 37 === b ? h.imgprev(!0) : 39 === b ? h.imgnext(!0) : 27 === b && f.close(h.index);
                }
            }, h.tabimg = function (a) {
                l.length <= 1 || (k.start = h.imgIndex - 1, f.close(h.index), f.photos(b, !0, a));
            }, h.event = function () {
                h.bigimg.hover(function () {
                    h.imgsee.show();
                }, function () {
                    h.imgsee.hide();
                }), h.bigimg.find(".layui-layer-imgprev").on("click", function (a) {
                    a.preventDefault(), h.imgprev();
                }), h.bigimg.find(".layui-layer-imgnext").on("click", function (a) {
                    a.preventDefault(), h.imgnext();
                }), c(document).on("keyup", h.keyup);
            }, h.loadi = f.load(1, {
                shade: "shade" in b ? !1 : .9,
                scrollbar: !1
            }), g(l[m].src, function (d) {
                f.close(h.loadi), h.index = f.open(c.extend({
                    type: 1,
                    area: function () {
                        var e = [d.width, d.height],
                            f = [c(a).width() - 50, c(a).height() - 50];
                        return !b.full && e[0] > f[0] && (e[0] = f[0], e[1] = e[0] * d.height / d.width), [e[0] + "px", e[1] + "px"];
                    }(),
                    title: !1,
                    shade: .9,
                    shadeClose: !0,
                    closeBtn: !1,
                    move: ".layui-layer-phimg img",
                    moveType: 1,
                    scrollbar: !1,
                    moveOut: !0,
                    shift: 5 * Math.random() | 0,
                    skin: "layui-layer-photos" + j("photos"),
                    content: '<div class="layui-layer-phimg"><img src="' + l[m].src + '" alt="' + (l[m].alt || "") + '" layer-pid="' + l[m].pid + '"><div class="layui-layer-imgsee">' + (l.length > 1 ? '<span class="layui-layer-imguide"><a href="javascript:;" class="layui-layer-iconext layui-layer-imgprev"></a><a href="javascript:;" class="layui-layer-iconext layui-layer-imgnext"></a></span>' : "") + '<div class="layui-layer-imgbar" style="display:' + (e ? "block" : "") + '"><span class="layui-layer-imgtit"><a href="javascript:;">' + (l[m].alt || "") + "</a><em>" + h.imgIndex + "/" + l.length + "</em></span></div></div></div>",
                    success: function (a, c) {
                        h.bigimg = a.find(".layui-layer-phimg"), h.imgsee = a.find(".layui-layer-imguide,.layui-layer-imgbar"),
                            h.event(a), b.tab && b.tab(l[m], a);
                    },
                    end: function () {
                        h.end = !0, c(document).off("keyup", h.keyup);
                    }
                }, b));
            }, function () {
                f.close(h.loadi), f.msg("&#x5F53;&#x524D;&#x56FE;&#x7247;&#x5730;&#x5740;&#x5F02;&#x5E38;<br>&#x662F;&#x5426;&#x7EE7;&#x7EED;&#x67E5;&#x770B;&#x4E0B;&#x4E00;&#x5F20;&#xFF1F;", {
                    time: 3e4,
                    btn: ["&#x4E0B;&#x4E00;&#x5F20;", "&#x4E0D;&#x770B;&#x4E86;"],
                    yes: function () {
                        l.length > 1 && h.imgnext(!0, !0);
                    }
                });
            });
        }
    }, e.run = function () {
        c = jQuery, d = c(a), h.html = c("html"), f.open = function (a) {
            var b = new g(a);
            return b.index;
        };
    }, "function" == typeof define ? define(function () {
        return e.run(), f;
    }) : function () {
        e.run(), f.use("http://static8.ziroom.com/phoenix/pc/css/layerskin/layer.css");
    }();
}(window);



;
(function ($) {
    $.fn.extend({
        t_downmenu_fn: function (options) {
            var defaults = {
                id: "name"
            };

            var options = $.extend(defaults, options);

            this.each(function () {
                var timer = null;
                $(this).find(".t_li").hover(function () {
                    clearTimeout(timer);
                    $(this).next().show();
                }, function () {
                    var _this = this;
                    timer = setTimeout(function () {
                        $(_this).next(".t_ul").hide();
                    }, 20)
                });
                $(this).find(".t_ul").hover(function () {
                    clearTimeout(timer);
                    $(this).show();

                }, function () {
                    $(this).hide();
                });
            }); //end each
        }
    });
})(jQuery);

/*
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/

*/
jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend(jQuery.easing, {
    def: 'easeOutQuad',
    swing: function (x, t, b, c, d) {
        //alert(jQuery.easing.default);
        return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
    },
    easeInQuad: function (x, t, b, c, d) {
        return c * (t /= d) * t + b;
    },
    easeOutQuad: function (x, t, b, c, d) {
        return -c * (t /= d) * (t - 2) + b;
    },
    easeInOutQuad: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t + b;
        return -c / 2 * ((--t) * (t - 2) - 1) + b;
    },
    easeInCubic: function (x, t, b, c, d) {
        return c * (t /= d) * t * t + b;
    },
    easeOutCubic: function (x, t, b, c, d) {
        return c * ((t = t / d - 1) * t * t + 1) + b;
    },
    easeInOutCubic: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
        return c / 2 * ((t -= 2) * t * t + 2) + b;
    },
    easeInQuart: function (x, t, b, c, d) {
        return c * (t /= d) * t * t * t + b;
    },
    easeOutQuart: function (x, t, b, c, d) {
        return -c * ((t = t / d - 1) * t * t * t - 1) + b;
    },
    easeInOutQuart: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
        return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
    },
    easeInQuint: function (x, t, b, c, d) {
        return c * (t /= d) * t * t * t * t + b;
    },
    easeOutQuint: function (x, t, b, c, d) {
        return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
    },
    easeInOutQuint: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
        return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
    },
    easeInSine: function (x, t, b, c, d) {
        return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
    },
    easeOutSine: function (x, t, b, c, d) {
        return c * Math.sin(t / d * (Math.PI / 2)) + b;
    },
    easeInOutSine: function (x, t, b, c, d) {
        return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
    },
    easeInExpo: function (x, t, b, c, d) {
        return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
    },
    easeOutExpo: function (x, t, b, c, d) {
        return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
    },
    easeInOutExpo: function (x, t, b, c, d) {
        if (t == 0) return b;
        if (t == d) return b + c;
        if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
        return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
    },
    easeInCirc: function (x, t, b, c, d) {
        return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
    },
    easeOutCirc: function (x, t, b, c, d) {
        return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
    },
    easeInOutCirc: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
        return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
    },
    easeInElastic: function (x, t, b, c, d) {
        var s = 1.70158;
        var p = 0;
        var a = c;
        if (t == 0) return b;
        if ((t /= d) == 1) return b + c;
        if (!p) p = d * .3;
        if (a < Math.abs(c)) {
            a = c;
            var s = p / 4;
        } else var s = p / (2 * Math.PI) * Math.asin(c / a);
        return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
    },
    easeOutElastic: function (x, t, b, c, d) {
        var s = 1.70158;
        var p = 0;
        var a = c;
        if (t == 0) return b;
        if ((t /= d) == 1) return b + c;
        if (!p) p = d * .3;
        if (a < Math.abs(c)) {
            a = c;
            var s = p / 4;
        } else var s = p / (2 * Math.PI) * Math.asin(c / a);
        return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
    },
    easeInOutElastic: function (x, t, b, c, d) {
        var s = 1.70158;
        var p = 0;
        var a = c;
        if (t == 0) return b;
        if ((t /= d / 2) == 2) return b + c;
        if (!p) p = d * (.3 * 1.5);
        if (a < Math.abs(c)) {
            a = c;
            var s = p / 4;
        } else var s = p / (2 * Math.PI) * Math.asin(c / a);
        if (t < 1) return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
        return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b;
    },
    easeInBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c * (t /= d) * t * ((s + 1) * t - s) + b;
    },
    easeOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
    },
    easeInOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b;
        return c / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b;
    },
    easeInBounce: function (x, t, b, c, d) {
        return c - jQuery.easing.easeOutBounce(x, d - t, 0, c, d) + b;
    },
    easeOutBounce: function (x, t, b, c, d) {
        if ((t /= d) < (1 / 2.75)) {
            return c * (7.5625 * t * t) + b;
        } else if (t < (2 / 2.75)) {
            return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b;
        } else if (t < (2.5 / 2.75)) {
            return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b;
        } else {
            return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b;
        }
    },
    easeInOutBounce: function (x, t, b, c, d) {
        if (t < d / 2) return jQuery.easing.easeInBounce(x, t * 2, 0, c, d) * .5 + b;
        return jQuery.easing.easeOutBounce(x, t * 2 - d, 0, c, d) * .5 + c * .5 + b;
    }
});



// 基于JQ的插件
// 创建一个闭包     
(function ($) {
    //插件主要内容 tab切换     
    $.fn.tab = function (options) {
        // 处理默认参数   
        var opts = $.extend({}, $.fn.tab.defaults, options);
        return this.each(function () {
            var $this = $(this),
                $tabNavObj = $(opts.tabNavObj, $this),
                $tabContentObj = $(opts.tabContentObj, $this),
                $tabNavBtns = $(opts.tabNavBtn, $tabNavObj),
                $tabContentBlocks = $(opts.tabContent, $tabContentObj);
            $tabNavBtns.bind(opts.eventType, function () {
                var $that = $(this),
                    _index = $tabNavBtns.index($that);

                if (!$that.hasClass('more')) {
                    $that.addClass(opts.currentClass).siblings(opts.tabNavBtn).removeClass(opts.currentClass);
                    $tabContentBlocks.eq(_index).show().siblings(opts.tabContent).hide();
                }

            }).eq(0).trigger(opts.eventType);
        });
        // 保存JQ的连贯操作结束
    };
    //插件主要内容结束

    // 插件的defaults     
    $.fn.tab.defaults = {
        tabNavObj: '.tabNav',
        tabNavBtn: 'li',
        tabContentObj: '.tabContent',
        tabContent: '.list',
        currentClass: 'current',
        eventType: 'click'
    };
    //tab 用法例：$(".index-tabWrap").tab({tabNavObj:".tabNav",tabContentObj:".tabContent",tabContent:".con",eventType:"click"}); 


    //hover show 插件
    $.fn.hoverShow = function (options) {
        var opts = $.extend({}, $.fn.hoverShow.defaults, options);
        return this.each(function () {
            var $this = $(this);

            $this.hover(function () {
                $this.addClass(opts.hoverObjClass);
            }, function () {
                $this.removeClass(opts.hoverObjClass);
            });
        });
    };
    //插件主要内容结束

    // 插件的defaults     
    $.fn.hoverShow.defaults = {
        hoverObj: '.menu_list',
        hoverObjClass: 'menuhover'
    };
    //hoverShow 用法例：$(".menu_list").hoverShow({hoverObj:".menu_list",hoverObjClass:"menuhover"}); 

    //tabBussiness 插件
    $.fn.tabBuss = function (options) {
        var opts = $.extend({}, $.fn.tabBuss.defaults, options);
        return this.each(function () {
            var $this = $(this),
                aDt = $this.find('dt'),
                aDD = $this.find('dd');

            aDt.each(function (i) {
                $(this).hover(function () {
                    aDt.removeClass();
                    $(this).addClass('active');
                    aDD.hide();
                    aDD.eq(i).show();
                });
            });


        });
    };
    //插件主要内容结束

    // 插件的defaults     
    $.fn.tabBuss.defaults = {
        obj: '#business'
    };
    //hoverShow 用法例：$(".menu_list").hoverShow({hoverObj:".menu_list",hoverObjClass:"menuhover"}); 


    $.fn.placeholder = function () {
        this.each(function () {
            var searchText = $(this);
            var searchValue = searchText.attr('placeholder');
            if (!('placeholder' in document.createElement('input'))) {
                searchText.removeAttr('placeholder').val(searchValue).bind('focus', function () {
                    if (this.value == searchValue) {
                        this.value = '';
                    };
                }).bind('blur', function () {
                    if (this.value == '') {
                        this.value = searchValue;
                    };
                });
            } else {
                searchText.bind('focus', function () {
                    if (searchText.attr('placeholder') == searchValue) {
                        searchText.attr('placeholder', '')
                    };
                }).bind('blur', function () {
                    if (searchText.attr('placeholder', '')) {
                        searchText.attr('placeholder', searchValue)
                    };
                });
            }
        });

    };
    //placeholder 用法例：$('.inp-bor').placeholder();

})(jQuery);
// 闭包结束
/*
图片延迟加载
 *
 */
(function (a, b, c, d) {
    var e = a(b);
    a.fn.lazyload = function (c) {
        function i() {
            var b = 0;
            f.each(function () {
                var c = a(this);
                if (h.skip_invisible && !c.is(":visible")) return;
                if (!a.abovethetop(this, h) && !a.leftofbegin(this, h))
                    if (!a.belowthefold(this, h) && !a.rightoffold(this, h)) c.trigger("appear"), b = 0;
                    else if (++b > h.failure_limit) return !1
            })
        }
        var f = this,
            g, h = {
                threshold: 0,
                failure_limit: 0,
                event: "scroll",
                effect: "show",
                container: b,
                data_attribute: "original",
                skip_invisible: !0,
                appear: null,
                load: null
            };
        return c && (d !== c.failurelimit && (c.failure_limit = c.failurelimit, delete c.failurelimit), d !== c.effectspeed && (c.effect_speed = c.effectspeed, delete c.effectspeed), a.extend(h, c)), g = h.container === d || h.container === b ? e : a(h.container), 0 === h.event.indexOf("scroll") && g.bind(h.event, function (a) {
            return i()
        }), this.each(function () {
            var b = this,
                c = a(b);
            b.loaded = !1, c.one("appear", function () {
                if (!this.loaded) {
                    if (h.appear) {
                        var d = f.length;
                        h.appear.call(b, d, h)
                    }
                    a("<img />").bind("load", function () {
                        c.hide().attr("src", c.data(h.data_attribute))[h.effect](h.effect_speed), b.loaded = !0;
                        var d = a.grep(f, function (a) {
                            return !a.loaded
                        });
                        f = a(d);
                        if (h.load) {
                            var e = f.length;
                            h.load.call(b, e, h)
                        }
                    }).attr("src", c.data(h.data_attribute))
                }
            }), 0 !== h.event.indexOf("scroll") && c.bind(h.event, function (a) {
                b.loaded || c.trigger("appear")
            })
        }), e.bind("resize", function (a) {
            i()
        }), /iphone|ipod|ipad.*os 5/gi.test(navigator.appVersion) && e.bind("pageshow", function (b) {
            b.originalEvent.persisted && f.each(function () {
                a(this).trigger("appear")
            })
        }), a(b).load(function () {
            i()
        }), this
    }, a.belowthefold = function (c, f) {
        var g;
        return f.container === d || f.container === b ? g = e.height() + e.scrollTop() : g = a(f.container).offset().top + a(f.container).height(), g <= a(c).offset().top - f.threshold
    }, a.rightoffold = function (c, f) {
        var g;
        return f.container === d || f.container === b ? g = e.width() + e.scrollLeft() : g = a(f.container).offset().left + a(f.container).width(), g <= a(c).offset().left - f.threshold
    }, a.abovethetop = function (c, f) {
        var g;
        return f.container === d || f.container === b ? g = e.scrollTop() : g = a(f.container).offset().top, g >= a(c).offset().top + f.threshold + a(c).height()
    }, a.leftofbegin = function (c, f) {
        var g;
        return f.container === d || f.container === b ? g = e.scrollLeft() : g = a(f.container).offset().left, g >= a(c).offset().left + f.threshold + a(c).width()
    }, a.inviewport = function (b, c) {
        return !a.rightoffold(b, c) && !a.leftofbegin(b, c) && !a.belowthefold(b, c) && !a.abovethetop(b, c)
    }, a.extend(a.expr[":"], {
        "below-the-fold": function (b) {
            return a.belowthefold(b, {
                threshold: 0
            })
        },
        "above-the-top": function (b) {
            return !a.belowthefold(b, {
                threshold: 0
            })
        },
        "right-of-screen": function (b) {
            return a.rightoffold(b, {
                threshold: 0
            })
        },
        "left-of-screen": function (b) {
            return !a.rightoffold(b, {
                threshold: 0
            })
        },
        "in-viewport": function (b) {
            return a.inviewport(b, {
                threshold: 0
            })
        },
        "above-the-fold": function (b) {
            return !a.belowthefold(b, {
                threshold: 0
            })
        },
        "right-of-fold": function (b) {
            return a.rightoffold(b, {
                threshold: 0
            })
        },
        "left-of-fold": function (b) {
            return !a.rightoffold(b, {
                threshold: 0
            })
        }
    })
})(jQuery, window, document);


//固定定位
$.fn.divfixed = function (options) {
    var opts = $.extend({}, options);
    return this.each(function () {
        var $this = $(this);
        var thisTop = $this.offset().top;
        var winTop = $(window).scrollTop();
        var aLi = $('li', $this);

        $(window).scroll(function () {
            winTop = $(window).scrollTop();
            if (winTop > thisTop) {
                $this.css({
                    "position": "fixed",
                    "top": opts.top
                });

                if ($this.attr('id') == 'divfixed') {
                    $this.css({
                        'visibility': 'visible'
                    });
                }

            } else {
                $this.css({
                    "position": "static",
                    "top": opts.top
                });

            }
        });



    });
};

//--------弹出层---------//

jQuery.fn.boxy = function (options) {
    options = options || {};
    return this.each(function () {
        var node = this.nodeName.toLowerCase(),
            self = this;
        if (node == 'a') {
            jQuery(this).click(function () {
                var active = Boxy.linkedTo(this),
                    href = this.getAttribute('href'),
                    localOptions = jQuery.extend({
                        actuator: this,
                        title: this.title
                    }, options);

                if (active) {
                    active.show();
                } else if (href.indexOf('#') >= 0) {
                    var content = jQuery(href.substr(href.indexOf('#'))),
                        newContent = content.clone(true);
                    content.remove();
                    localOptions.unloadOnHide = false;
                    new Boxy(newContent, localOptions);
                } else { // fall back to AJAX; could do with a same-origin check
                    if (!localOptions.cache) localOptions.unloadOnHide = true;
                    Boxy.load(this.href, localOptions);
                }

                return false;
            });
        } else if (node == 'form') {
            jQuery(this).bind('submit.boxy', function () {
                Boxy.confirm(options.message || '请确认：', function () {
                    jQuery(self).unbind('submit.boxy').submit();
                });
                return false;
            });
        }
    });
};

//
// Boxy Class

function Boxy(element, options) {

    this.boxy = jQuery(Boxy.WRAPPER);
    jQuery.data(this.boxy[0], 'boxy', this);

    this.visible = false;
    this.options = jQuery.extend({}, Boxy.DEFAULTS, options || {});

    if (this.options.modal) {
        this.options = jQuery.extend(this.options, {
            center: true,
            draggable: false
        });
    }

    // options.actuator == DOM element that opened this boxy
    // association will be automatically deleted when this boxy is remove()d
    if (this.options.actuator) {
        jQuery.data(this.options.actuator, 'active.boxy', this);
    }

    this.setContent(element || "<div></div>");
    this._setupTitleBar();

    this.boxy.css('display', 'none').appendTo(document.body);
    this.toTop();

    if (this.options.fixed) {
        if (jQuery.browser.msie && jQuery.browser.version < 7) {
            this.options.fixed = false; // IE6 doesn't support fixed positioning
        } else {
            this.boxy.addClass('fixed');
        }
    }

    if (this.options.center && Boxy._u(this.options.x, this.options.y)) {
        this.center();
    } else {
        this.moveTo(
            Boxy._u(this.options.x) ? this.options.x : Boxy.DEFAULT_X,
            Boxy._u(this.options.y) ? this.options.y : Boxy.DEFAULT_Y
        );
    }

    if (this.options.show) this.show();

};

Boxy.EF = function () {};

jQuery.extend(Boxy, {

    WRAPPER: "<table cellspacing='0' cellpadding='0' border='0' class='boxy-wrapper' style='position:fixed; _position: absolute;'>" +
        "<tr><td class='top-left'></td><td class='top'></td><td class='top-right'></td></tr>" +
        "<tr><td class='left'></td><td class='boxy-inner'></td><td class='right'></td></tr>" +
        "<tr><td class='bottom-left'></td><td class='bottom'></td><td class='bottom-right'></td></tr>" +
        "</table>",

    DEFAULTS: {
        title: null, // titlebar text. titlebar will not be visible if not set.
        closeable: true, // display close link in titlebar?
        draggable: true, // can this dialog be dragged?
        clone: false, // clone content prior to insertion into dialog?
        actuator: null, // element which opened this dialog
        center: true, // center dialog in viewport?
        show: true, // show dialog immediately?
        modal: true, // make dialog modal?
        fixed: true, // use fixed positioning, if supported? absolute positioning used otherwise
        closeText: '关闭', // text to use for default close link
        unloadOnHide: false, // should this dialog be removed from the DOM after being hidden?
        clickToFront: false, // bring dialog to foreground on any click (not just titlebar)?
        behaviours: Boxy.EF, // function used to apply behaviours to all content embedded in dialog.
        afterDrop: Boxy.EF, // callback fired after dialog is dropped. executes in context of Boxy instance.
        afterShow: Boxy.EF, // callback fired after dialog becomes visible. executes in context of Boxy instance.
        afterHide: Boxy.EF, // callback fired after dialog is hidden. executed in context of Boxy instance.
        beforeUnload: Boxy.EF // callback fired after dialog is unloaded. executed in context of Boxy instance.
    },

    DEFAULT_X: 50,
    DEFAULT_Y: 50,
    zIndex: 1337,
    dragConfigured: false, // only set up one drag handler for all boxys
    resizeConfigured: false,
    dragging: null,

    // load a URL and display in boxy
    // url - url to load
    // options keys (any not listed below are passed to boxy constructor)
    //   type: HTTP method, default: GET
    //   cache: cache retrieved content? default: false
    //   filter: jQuery selector used to filter remote content
    load: function (url, options) {

        options = options || {};

        var ajax = {
            url: url,
            type: 'GET',
            dataType: 'html',
            cache: false,
            success: function (html) {
                html = jQuery(html);
                if (options.filter) html = jQuery(options.filter, html);
                new Boxy(html, options);
            }
        };

        jQuery.each(['type', 'cache'], function () {
            if (this in options) {
                ajax[this] = options[this];
                delete options[this];
            }
        });

        jQuery.ajax(ajax);

    },

    // allows you to get a handle to the containing boxy instance of any element
    // e.g. <a href='#' onclick='alert(Boxy.get(this));'>inspect!</a>.
    // this returns the actual instance of the boxy 'class', not just a DOM element.
    // Boxy.get(this).hide() would be valid, for instance.
    get: function (ele) {
        var p = jQuery(ele).parents('.boxy-wrapper');
        return p.length ? jQuery.data(p[0], 'boxy') : null;
    },

    // returns the boxy instance which has been linked to a given element via the
    // 'actuator' constructor option.
    linkedTo: function (ele) {
        return jQuery.data(ele, 'active.boxy');
    },

    // displays an alert box with a given message, calling optional callback
    // after dismissal.
    alert: function (message, callback, options) {
        return Boxy.ask(message, ['确认'], callback, options);
    },

    // displays an alert box with a given message, calling after callback iff
    // user selects OK.
    confirm: function (message, after, options) {
        return Boxy.ask(message, ['确认', '取消'], function (response) {
            if (response == '确认') after();
        }, options);
    },

    // asks a question with multiple responses presented as buttons
    // selected item is returned to a callback method.
    // answers may be either an array or a hash. if it's an array, the
    // the callback will received the selected value. if it's a hash,
    // you'll get the corresponding key.
    ask: function (question, answers, callback, options) {

        options = jQuery.extend({
                modal: true,
                closeable: false
            },
            options || {}, {
                show: true,
                unloadOnHide: true
            });

        var body = jQuery('<div></div>').append(jQuery('<div class="question"></div>').html(question));

        // ick
        var map = {},
            answerStrings = [];
        if (answers instanceof Array) {
            for (var i = 0; i < answers.length; i++) {
                map[answers[i]] = answers[i];
                answerStrings.push(answers[i]);
            }
        } else {
            for (var k in answers) {
                map[answers[k]] = k;
                answerStrings.push(answers[k]);
            }
        }

        var buttons = jQuery('<form class="answers"></form>');
        buttons.html(jQuery.map(answerStrings, function (v) {
            //add by zhangxinxu 给确认对话框的确认取消按钮添加不同的class
            var btn_index;
            if (v === "确认") {
                btn_index = 1;
            } else if (v === "取消") {
                btn_index = 2;
            } else {
                btn_index = 3;
            }
            //add end.  include the 'btn_index' below 
            return "<input class='boxy-btn" + btn_index + "' type='button' value='" + v + "' />";
        }).join(' '));

        jQuery('input[type=button]', buttons).click(function () {
            var clicked = this;
            Boxy.get(this).hide(function () {
                if (callback) callback(map[clicked.value]);
            });
        });

        body.append(buttons);

        new Boxy(body, options);

    },

    // returns true if a modal boxy is visible, false otherwise
    isModalVisible: function () {
        return jQuery('.boxy-modal-blackout').length > 0;
    },

    _u: function () {
        for (var i = 0; i < arguments.length; i++)
            if (typeof arguments[i] != 'undefined') return false;
        return true;
    },

    _handleResize: function (evt) {
        var d = jQuery(document);
        jQuery('.boxy-modal-blackout').css('display', 'none').css({
            width: d.width(),
            height: d.height()
        }).css('display', 'block');

    },

    _handleDrag: function (evt) {
        var d;
        if (d = Boxy.dragging) {
            d[0].boxy.css({
                left: evt.pageX - d[1],
                top: evt.pageY - d[2]
            });
        }
    },

    _nextZ: function () {
        return Boxy.zIndex++;
    },

    _viewport: function () {

        var d = document.documentElement,
            b = document.body,
            w = window;
        $(window).resize(function () {
            var left = $('.boxy-wrapper').outerWidth() / 2;
            var top = $('.boxy-wrapper').outerHeight() / 2

            $('.boxy-wrapper').css({
                "left": "50%",
                "margin-left": -left + "px",
                "top": "50%",
                "margin-top": -top + "px"
            });
        });
        return jQuery.extend(
            jQuery.browser.msie ? {
                left: b.scrollLeft || d.scrollLeft,
                top: b.scrollTop || d.scrollTop
            } : {
                left: w.pageXOffset,
                top: w.pageYOffset
            }, !Boxy._u(w.innerWidth) ? {
                width: w.innerWidth,
                height: w.innerHeight
            } :
            (!Boxy._u(d) && !Boxy._u(d.clientWidth) && d.clientWidth != 0 ? {
                width: d.clientWidth,
                height: d.clientHeight
            } : {
                width: b.clientWidth,
                height: b.clientHeight
            }));

    }

});

Boxy.prototype = {

    // Returns the size of this boxy instance without displaying it.
    // Do not use this method if boxy is already visible, use getSize() instead.
    estimateSize: function () {
        this.boxy.css({
            visibility: 'hidden',
            display: 'block'
        });
        var dims = this.getSize();
        this.boxy.css('display', 'none').css('visibility', 'visible');
        return dims;
    },

    // Returns the dimensions of the entire boxy dialog as [width,height]
    getSize: function () {
        return [this.boxy.width(), this.boxy.height()];
    },

    // Returns the dimensions of the content region as [width,height]
    getContentSize: function () {
        var c = this.getContent();
        return [c.width(), c.height()];
    },

    // Returns the position of this dialog as [x,y]
    getPosition: function () {
        var b = this.boxy[0];
        return [b.offsetLeft, b.offsetTop];
    },

    // Returns the center point of this dialog as [x,y]
    getCenter: function () {
        var p = this.getPosition();
        var s = this.getSize();
        return [Math.floor(p[0] + s[0] / 2), Math.floor(p[1] + s[1] / 2)];
    },

    // Returns a jQuery object wrapping the inner boxy region.
    // Not much reason to use this, you're probably more interested in getContent()
    getInner: function () {
        return jQuery('.boxy-inner', this.boxy);
    },

    // Returns a jQuery object wrapping the boxy content region.
    // This is the user-editable content area (i.e. excludes titlebar)
    getContent: function () {
        return jQuery('.boxy-content', this.boxy);
    },

    // Replace dialog content
    setContent: function (newContent) {
        newContent = jQuery(newContent).css({
            display: 'block'
        }).addClass('boxy-content');
        if (this.options.clone) newContent = newContent.clone(true);
        this.getContent().remove();
        this.getInner().append(newContent);
        this._setupDefaultBehaviours(newContent);
        this.options.behaviours.call(this, newContent);
        return this;
    },

    // Move this dialog to some position, funnily enough
    moveTo: function (x, y) {
        this.moveToX(x).moveToY(y);
        return this;
    },

    // Move this dialog (x-coord only)
    moveToX: function (x) {
        if (typeof x == 'number') this.boxy.css({
            left: x
        });
        else this.centerX();
        return this;
    },

    // Move this dialog (y-coord only)
    moveToY: function (y) {
        if (typeof y == 'number') this.boxy.css({
            top: y
        });
        else this.centerY();
        return this;
    },

    // Move this dialog so that it is centered at (x,y)
    centerAt: function (x, y) {
        var s = this[this.visible ? 'getSize' : 'estimateSize']();
        if (typeof x == 'number') this.moveToX(x - s[0] / 2);
        if (typeof y == 'number') this.moveToY(y - s[1] / 2);
        return this;
    },

    centerAtX: function (x) {
        return this.centerAt(x, null);
    },

    centerAtY: function (y) {
        return this.centerAt(null, y);
    },

    // Center this dialog in the viewport
    // axis is optional, can be 'x', 'y'.
    center: function (axis) {
        var v = Boxy._viewport();
        var o = this.options.fixed ? [0, 0] : [v.left, v.top];
        if (!axis || axis == 'x') this.centerAt(o[0] + v.width / 2, null);
        if (!axis || axis == 'y') this.centerAt(null, o[1] + v.height / 2);
        return this;
    },

    // Center this dialog in the viewport (x-coord only)
    centerX: function () {
        return this.center('x');
    },

    // Center this dialog in the viewport (y-coord only)
    centerY: function () {
        return this.center('y');
    },

    // Resize the content region to a specific size
    resize: function (width, height, after) {

        if (!this.visible) return;
        var bounds = this._getBoundsForResize(width, height);
        this.boxy.css({
            left: bounds[0],
            top: bounds[1]
        });
        this.getContent().css({
            width: bounds[2],
            height: bounds[3]
        });
        if (after) after(this);
        return this;
    },

    // Tween the content region to a specific size
    tween: function (width, height, after) {
        if (!this.visible) return;
        var bounds = this._getBoundsForResize(width, height);
        var self = this;
        this.boxy.stop().animate({
            left: bounds[0],
            top: bounds[1]
        });
        this.getContent().stop().animate({
            width: bounds[2],
            height: bounds[3]
        }, function () {
            if (after) after(self);
        });
        return this;
    },

    // Returns true if this dialog is visible, false otherwise
    isVisible: function () {
        return this.visible;
    },

    // Make this boxy instance visible
    show: function () {
        if (this.visible) return;
        if (this.options.modal) {
            var self = this;
            if (!Boxy.resizeConfigured) {
                Boxy.resizeConfigured = true;
                jQuery(window).resize(function () {
                    Boxy._handleResize();
                });
            }
            this.modalBlackout = jQuery('<div class="boxy-modal-blackout"></div>')
                .css({
                    zIndex: Boxy._nextZ(),
                    opacity: 0.5,
                    width: jQuery(document).width(),
                    height: jQuery(document).height()
                })
                .appendTo(document.body);
            this.toTop();
            if (this.options.closeable) {
                jQuery(document.body).bind('keypress.boxy', function (evt) {
                    var key = evt.which || evt.keyCode;
                    if (key == 27) {
                        self.hide();
                        jQuery(document.body).unbind('keypress.boxy');
                    }
                });
            }
        }
        this.boxy.stop().css({
            opacity: 1
        }).show();
        this.visible = true;
        this._fire('afterShow');
        return this;
    },

    // Hide this boxy instance
    hide: function (after) {
        if (!this.visible) return;
        var self = this;
        if (this.options.modal) {
            jQuery(document.body).unbind('keypress.boxy');
            this.modalBlackout.animate({
                opacity: 0
            }, function () {
                jQuery(this).remove();
            });
        }
        this.boxy.stop().animate({
            opacity: 0
        }, 300, function () {
            self.boxy.css({
                display: 'none'
            });
            self.visible = false;
            self._fire('afterHide');
            if (after) after(self);
            if (self.options.unloadOnHide) self.unload();
        });
        return this;
    },

    toggle: function () {
        this[this.visible ? 'hide' : 'show']();
        return this;
    },

    hideAndUnload: function (after) {
        this.options.unloadOnHide = true;
        this.hide(after);
        return this;
    },

    unload: function () {
        this._fire('beforeUnload');
        this.boxy.remove();
        if (this.options.actuator) {
            jQuery.data(this.options.actuator, 'active.boxy', false);
        }
    },

    // Move this dialog box above all other boxy instances
    toTop: function () {
        this.boxy.css({
            zIndex: Boxy._nextZ()
        });
        return this;
    },

    // Returns the title of this dialog
    getTitle: function () {
        return jQuery('> .title-bar h2', this.getInner()).html();
    },

    // Sets the title of this dialog
    setTitle: function (t) {
        jQuery('> .title-bar h2', this.getInner()).html(t);
        return this;
    },

    //
    // Don't touch these privates

    _getBoundsForResize: function (width, height) {
        var csize = this.getContentSize();
        var delta = [width - csize[0], height - csize[1]];
        var p = this.getPosition();
        return [Math.max(p[0] - delta[0] / 2, 0),
            Math.max(p[1] - delta[1] / 2, 0), width, height
        ];
    },

    _setupTitleBar: function () {
        if (this.options.title) {
            var self = this;
            var tb = jQuery("<div class='title-bar'></div>").html("<h2>" + this.options.title + "</h2>");
            if (this.options.closeable) {
                tb.append(jQuery("<a href='#' class='close'></a>").html(this.options.closeText));
            }
            if (this.options.draggable) {
                tb[0].onselectstart = function () {
                    return false;
                }
                tb[0].unselectable = 'on';
                tb[0].style.MozUserSelect = 'none';
                if (!Boxy.dragConfigured) {
                    jQuery(document).mousemove(Boxy._handleDrag);
                    Boxy.dragConfigured = true;
                }
                tb.mousedown(function (evt) {
                    self.toTop();
                    Boxy.dragging = [self, evt.pageX - self.boxy[0].offsetLeft, evt.pageY - self.boxy[0].offsetTop];
                    jQuery(this).addClass('dragging');
                }).mouseup(function () {
                    jQuery(this).removeClass('dragging');
                    Boxy.dragging = null;
                    self._fire('afterDrop');
                });
            }
            this.getInner().prepend(tb);
            this._setupDefaultBehaviours(tb);
        }
    },

    _setupDefaultBehaviours: function (root) {
        var self = this;
        if (this.options.clickToFront) {
            root.click(function () {
                self.toTop();
            });
        }
        jQuery('.close', root).click(function () {
            self.hide();
            return false;
        }).mousedown(function (evt) {
            evt.stopPropagation();
        });
    },

    _fire: function (event) {
        this.options[event].call(this);
    }

};


/*关闭弹出层*/
function closeboxy() {
    $('.boxy-wrapper .title-bar .close').click();
}


/*打开ID的弹层*/
function showBoxy(id, title) {
    boxyClose();
    var dialog = new Boxy(id, {
        title: title
    });
}


/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD (Register as an anonymous module)
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var pluses = /\+/g;

    function encode(s) {
        return config.raw ? s : encodeURIComponent(s);
    }

    function decode(s) {
        return config.raw ? s : decodeURIComponent(s);
    }

    function stringifyCookieValue(value) {
        return encode(config.json ? JSON.stringify(value) : String(value));
    }

    function parseCookieValue(s) {
        if (s.indexOf('"') === 0) {
            // This is a quoted cookie as according to RFC2068, unescape...
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        }

        try {
            // Replace server-side written pluses with spaces.
            // If we can't decode the cookie, ignore it, it's unusable.
            // If we can't parse the cookie, ignore it, it's unusable.
            s = decodeURIComponent(s.replace(pluses, ' '));
            return config.json ? JSON.parse(s) : s;
        } catch (e) {}
    }

    function read(s, converter) {
        var value = config.raw ? s : parseCookieValue(s);
        return $.isFunction(converter) ? converter(value) : value;
    }

    var config = $.cookie = function (key, value, options) {

        // Write

        if (arguments.length > 1 && !$.isFunction(value)) {
            options = $.extend({}, config.defaults, options);

            if (typeof options.expires === 'number') {
                var days = options.expires,
                    t = options.expires = new Date();
                t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
            }

            return (document.cookie = [
                encode(key), '=', stringifyCookieValue(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path ? '; path=' + options.path : '',
                options.domain ? '; domain=' + options.domain : '',
                options.secure ? '; secure' : ''
            ].join(''));
        }

        // Read

        var result = key ? undefined : {},
            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling $.cookie().
            cookies = document.cookie ? document.cookie.split('; ') : [],
            i = 0,
            l = cookies.length;

        for (; i < l; i++) {
            var parts = cookies[i].split('='),
                name = decode(parts.shift()),
                cookie = parts.join('=');

            if (key === name) {
                // If second argument (value) is a function it's a converter...
                result = read(cookie, value);
                break;
            }

            // Prevent storing a cookie that we couldn't decode.
            if (!key && (cookie = read(cookie)) !== undefined) {
                result[name] = cookie;
            }
        }

        return result;
    };

    config.defaults = {};

    $.removeCookie = function (key, options) {
        // Must not alter options, thus extending a fresh object...
        $.cookie(key, '', $.extend({}, options, {
            expires: -1
        }));
        return !$.cookie(key);
    };

})); //jquery cookie


$(function () {
    $("#feedbackTextatea").focus(function () {
        $("#feedTips").hide();
    });
    $("#zxMsg").focus(function () {
        $("#z_msg_tips").hide();
    });

    /*$("input:text,textarea").blur(function(){
        var reg=/['")><&\\\/\.]/;
        if(reg.test($(this).val())){
            alert("输入内容含非法字符，请重新输入");
            $(this).val("");
        }
    });*/

    var w_top = 0;
    w_top = $(window).scrollTop();
    if (w_top > 60) {
        $('#slideBar').stop().animate({
            'right': 0
        }, 300);
    } else {
        $('#slideBar').stop().animate({
            'right': '-35px'
        }, 300);
    }
    $(window).scroll(function () {
        w_top = $(window).scrollTop();
        if (w_top > 60) {
            $('#slideBar').stop().animate({
                'right': 0
            }, 300);
        } else {
            $('#slideBar').stop().animate({
                'right': '-35px'
            }, 300);
        }
    });



    $('#slideBar li').hover(function () {
        $(this).find('.tab-text').css({
            'right': '-140px'
        }).stop().animate({
            'right': '35px'
        }, 300);

    }, function () {
        $(this).find('.tab-text').stop().animate({
            'right': '-140px'
        }, 300);
    });





    $('#goTop').click(function () {
        $('#slideBar').stop().animate({
            'right': '-35px'
        }, 300);
        $('.tab-text').stop().animate({
            'right': '-140px'
        }, 300);
        $(window).scrollTop(0);
    });



    var curUrl = window.location.href;

    if (curUrl.indexOf('/z/vr/') > 1 || curUrl.indexOf('/ditu/') > 1) {

    } else {
        $.cookie('mapType', ' ', {
            expires: 1,
            path: '/'
        });
    }





    showImgList();

});


function showImgList() {

    var _src = '';
    var iWinHeight = $(window).height();
    var top = $(window).scrollTop() + iWinHeight;


    $('.loadImg').each(function () {
        var _top = $(this).offset().top;
        if (_top < top) {
            _src = $(this).attr('_src');

            if (_src) {
                $(this).attr('src', _src).addClass('loadImgShow')

            }
        }
    });



    $(window).scroll(function () {
        top = $(window).scrollTop() + iWinHeight;
        $('.loadImg').each(function () {
            var _top = $(this).offset().top;
            if (_top < top) {
                _src = $(this).attr('_src');

                if (_src) {
                    $(this).attr('src', _src).addClass('loadImgShow')

                }
            }
        });




    });

}





function getUrlParameter(strParame) {
    var args = new Object();
    var query = location.search.substring(1);

    var pairs = query.split("&");
    for (var i = 0; i < pairs.length; i++) {
        var pos = pairs[i].indexOf('=');
        if (pos == -1) continue;
        var argname = pairs[i].substring(0, pos);
        var value = pairs[i].substring(pos + 1);
        value = decodeURIComponent(value);
        args[argname] = value;
    }
    return args[strParame];
}










var ZIROOM = {};

//获取当前地址
ZIROOM.getCurUrl = function () {
    var curWwwPath = window.document.location.href;
    return curWwwPath;
}


//获取接口地址
ZIROOM.getJsonUrl = function (variableName) {
    return variableName.url;
}


ZIROOM.ajaxPostSubmit = function (url, data, callback) {
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: data,
        success: function (result) {
            callback(result);
        },
        error: function (result) {
            //alert("error:"+result);
        }
    });
}

ZIROOM.ajaxGetSubmit = function (url, callback) {
    $.ajax({
        type: "GET",
        url: url,
        success: function (result) {
            callback(result);
        },
        error: function (result) {
            //alert("error:"+result);
        }
    });
}



//搜索联想
ZIROOM.SearchList = function (input, box, button, url) {

    var step = -1;
    var oldValue = "";
    var listTimer = null;
    var oInput = $(input),
        oBox = $(box),
        oUl = oBox.find('ul'),
        aLi = oUl.find('li'),
        oButton = $(button);

    $(document).click(function () {
        oBox.hide();
    });

    oInput.keyup(function (ev) {

        var oEvent = ev || event;
        var keyCode = oEvent.keyCode;
        var oTxt = $.trim(oInput.val());

        if (oTxt == '') {
            return;
        }

        if (oEvent.keyCode == 38 || oEvent.keyCode == 40) {
            return;
        }

        if (oEvent.keyCode == 13) {

            $(button).click();
        }
        clearInterval(listTimer);

        listTimer = setTimeout(function () {
            $.ajax({
                type: "GET",
                dataType: 'json',
                url: url,
                data: {
                    qwd: oTxt
                },
                success: function (json) {
                    var msg = json.data;
                    if (msg.length == 0) {
                        oBox.hide();
                        return false;
                    }
                    var ulstr = '';

                    oUl.html(ulstr);
                    for (var i = 0; i < msg.length; i++) {

                        if (!msg[i].name) {
                            oBox.hide();
                            return;
                        }
                        var goUrl = ZRCONFIG.URL_LIST;
                        var txt = '';

                        ulstr += '<li data="' + goUrl + '"><span class="keyword" style="margin-right:5px;">' + msg[i].name + '</span>' + msg[i].subname;
                        ulstr += txt;
                    }
                    oUl.html(ulstr).end().show();
                }
            });
        }, 500);
        oldValue = oTxt;
        step = -1;
    });

    oInput.keydown(function (ev) {
        var oEvent = ev || event;
        var keyCode = oEvent.keyCode;

        if (oEvent.keyCode == 40) //↓
        {
            var aLi = oBox.find('ul').find('li');
            step++;
            if (step == aLi.length) {
                step = -1;
            }
            aLi.removeClass("keyhover");
            if (step != -1) {
                aLi.eq(step).addClass("keyhover");
                oInput.attr('_url', aLi.eq(step).attr('data'));
                oInput.val(aLi.eq(step).find(".keyword").text());
            } else {
                oInput.val(oldValue);
                oInput.attr('_url', '');
            }
        } else if (oEvent.keyCode == 38) {
            var aLi = oBox.find('ul').find('li');
            step--;

            if (step == -2) {
                step = aLi.length - 1;
            }

            aLi.removeClass("keyhover");

            if (step != -1) {
                aLi.eq(step).addClass("keyhover");
                oInput.attr('_url', aLi.eq(step).attr('data'));
                oInput.val(aLi.eq(step).find(".keyword").text());
            } else {
                oInput.val(oldValue);
                oInput.attr('_url', '');
            }
        }
        if (oEvent.keyCode == 38 || oEvent.keyCode == 40) {
            return false;
        }
    });

    oBox.find("ul li").live("click", function () {
        var s_url = '';
        if ($(this).attr('data') != '') {
            s_url = "" + $(this).attr('data') + '?qwd=' + encodeURIComponent($(this).find(".keyword").text());
        } else {
            s_url = "/z/nl/?qwd=" + encodeURIComponent($(this).find(".keyword").text());
        }
        window.location = s_url;
    });

    aLi.live("mouseover mouseout", function (event) {
        aLi.removeClass("keyhover");
        if (event.type == 'mouseover') {
            $(this).addClass("keyhover");
            oInput.attr('_url', $(this).attr('data'));
            step = $(this).index();
        } else {
            $(this).removeClass("keyhover");
            oInput.attr('_url', '');
            step = $(this).index();
        }
    });


    $(button).click(function () {
        var $this = $(this);
        var iq = oInput.val();
        if (iq == "请输入地名，地铁线路，站点名..." || iq == "") return;
        var _url = oInput.attr('_url');
        if (typeof (_url) != 'undefined' && _url != '') {
            var zr = "" + oInput.attr('_url');
        } else {
            var zr = ZRCONFIG.URL_LIST;
            var query = zr + "?qwd=" + encodeURI($("#i_q_keyword_" + $this.attr("name")).val());
            window.location.href = query;
        }
    });
    oInput.keydown(function (e) {
        if (e.keyCode == 13) {
            var iq = oInput.val();
            if (iq == "请输入地名，地铁线路，站点名..." || iq == "") return;
            var zr = ZRCONFIG.URL_LIST;
            var query = zr + "?qwd=" + encodeURI(oInput.val());
            window.location.href = query;
        }

    })

}
ZIROOM.footer = function () {
    var oLinksList = $('#linksFooterList');
    var iLinksHeight = oLinksList.outerHeight();
    var iNum = iLinksHeight / 47;
    var iN = 0;

    setInterval(function () {
            iN++;
            iN = iN % iNum;
            var top = iN * 47;
            if (top >= iLinksHeight / 2 + 47) {
                oLinksList.css({
                    'margin-top': '0px'
                });
                iN = 0;
            } else {
                oLinksList.animate({
                        'margin-top': -iN * 47 + 'px'
                    },
                    1000);
            }

        },
        3000);
}

ZIROOM.changeCity = function () {
    var curUrl = window.location.href;
    var oCurrentCity = $('#current_city');
    var oCityList = $('#cityList');
    var cityArr = [{
            name: '北京',
            cityCode: '110000',
            url: 'http://www.ziroom.com'
        },
        {
            name: '上海',
            cityCode: '310000',
            url: 'http://sh.ziroom.com'
        },
        {
            name: '深圳',
            cityCode: '440300',
            url: 'http://sz.ziroom.com'
        },
        {
            name: '杭州',
            cityCode: '330100',
            url: 'http://hz.ziroom.com'
        },
        {
            name: '南京',
            cityCode: '320100',
            url: 'http://nj.ziroom.com'
        }
    ];
    var cityIndex = 0;

    if (curUrl.indexOf('www.ziroom.com') >= 0) {
        cityIndex = 0;
    } else if (curUrl.indexOf('sh.ziroom.com') >= 0) {
        cityIndex = 1;
    } else if (curUrl.indexOf('sz.ziroom.com') >= 0) {
        cityIndex = 2;
    } else if (curUrl.indexOf('hz.ziroom.com') >= 0) {
        cityIndex = 3;
    } else if (curUrl.indexOf('nj.ziroom.com') >= 0) {
        cityIndex = 4;
    }
    oCurrentCity.html(cityArr[cityIndex].name);
    var cityHtml = '';
    var cityActive = '';
    var a = new Date();
    //var b;
    for (var i = 0; i < cityArr.length; i++) {
        if (i == cityIndex) {
            cityActive = 'active';
        } else {
            cityActive = '';
        }
        //      if(i==0){
        //              b = '?t='+a.getSeconds();
        //          }else{
        //              b=''
        //          }
        cityHtml += '<a href="' + cityArr[i].url + '" data-id="' + cityArr[i].cityCode + '" class="' + cityActive + '">' + cityArr[i].name + '</a>'

    }
    oCityList.html(cityHtml);
}


ZIROOM.slideBar = function () {
    var w_top = 0;
    w_top = $(window).scrollTop(),
        oSlideBar = $('#slideBar');

    if (w_top > 60) {
        oSlideBar.stop().animate({
            'right': 0
        }, 300);
    } else {
        oSlideBar.stop().animate({
            'right': '-35px'
        }, 300);
    }
    $(window).scroll(function () {
        w_top = $(window).scrollTop();
        if (w_top > 60) {
            oSlideBar.stop().animate({
                'right': 0
            }, 300);
        } else {
            oSlideBar.stop().animate({
                'right': '-35px'
            }, 300);
        }
    });



    $('#slideBar li').hover(function () {
        $(this).find('.tab-text').css({
            'right': '-140px'
        }).stop().animate({
            'right': '35px'
        }, 300);

    }, function () {
        $(this).find('.tab-text').stop().animate({
            'right': '-140px'
        }, 300);
    });

    var fankuiHTML = '<div id="feedbackBox"  class="feedbackBox">' +
        '<textarea id="feedbackTextatea" placeholder="请输入您要发送的内容" maxlength="200"></textarea>' +
        '<p class="invalid" id="feedTips">反馈意见不能为空！</p>' +
        '<div class="tc mb10">' +
        '<input type="button" onclick="sub_feedback(this);" class="org_btn"  value="提 交">' +
        '</div>' +
        '</div>';


    $('#fankui').click(function () {
        layer.open({
            type: 1,
            area: ['350px', 'auto'], //宽高
            title: '意见反馈',
            content: fankuiHTML
        });
    });

    $('#goTop').click(function () {
        oSlideBar.stop().animate({
            'right': '-35px'
        }, 300);
        $('.tab-text').stop().animate({
            'right': '-140px'
        }, 300);
        $(window).scrollTop(0);
    });
}
ZIROOM.ifLogin = function () {


    var oUl = $('#topRightList');
    ZIROOM.ajaxGetSubmit('/user/check-login?url=' + window.location.href, function (data) {
        if (data) {
            var arr = data.data;
            if (arr.length > 0) {
                //已登录
                $('#ziroom_login').remove();
                $('#ziroom_reg').remove();
                var _html = '';

                for (var i = 0; i < arr.length - 1; i++) {
                    _html += '<a href="' + arr[i].url + '">' + arr[i].name + '</a>'
                }

                var oLi = $('<li class="people" id="people">' +
                    '<a href="http://i.ziroom.com/" class="user"></a>' +
                    '<div class="con">' + _html +
                    '</div>' +
                    '</li>');
                oUl.append(oLi);
                $('#slideBar .u a').attr('href', 'http://i.ziroom.com/');
                $('#slideBar .u .tab-text').html('个人中心');
                $('#slideBar .y a').attr('href', 'http://i.ziroom.com/?uri=contract/pay');


                if (ZRCONFIG.PAGE === 'detail') {

                    // $('#user_uid').val(arr[4].user_info.uid);
                    // $('#ly_name').val(arr[4].user_info.username);
                    // $('#ly_phone').val(arr[4].user_info.phone);

                    //详情页，点击约看登陆成功
                    if (getCookie('bookEnter') == '1') {
                        showYuekan();
                        setCookie('bookEnter', '0', 1);
                    }

                    $('#zreserve,#toCollect').attr('href', 'javascript:;');
                    $('#toCollect').click(function () { //收藏按扭
                        collect($('#room_id').val(), 0);
                    });
                    $('#zreserve').click(function () { //约看按扭
                        if (!$(this).hasClass('viewGray')) {
                            showYuekan();
                        }

                    });
                }
            } else {
                $('#ziroom_login,#ziroom_reg').css({
                    opacity: 1
                });
                if (!$('#zreserve').hasClass('viewGray')) {
                    //$('#toCollect').attr('href','http://passport.ziroom.com/login.html?return_url='+encodeURI(window.location.href));
                    //ie浏览器和其他浏览器区分
                    if (navigator.userAgent.indexOf('MSIE') >= 0) {
                        $('#zreserve').attr('href', 'http://passport.ziroom.com/login.html?return_url=' + encodeURI(window.location.href));
                        $('#toCollect').attr('href', 'http://passport.ziroom.com/login.html?return_url=' + encodeURI(window.location.href));
                    } else {
                        $('#zreserve').click(function () {
                            setCookie('bookEnter', '1', 1);
                            openExpressSignIn();
                        });
                        $('#toCollect').click(function () {
                            setCookie('bookEnter', '0', 1);
                            openExpressSignIn();
                        });
                    }

                }
                $('#toCollect').removeClass('collectEnd');
            }
        }
    });
};

ZIROOM.setQrcode = function () {


    var oImg = $('#topRightList .app .box img');
    var oImgmin = $('.glbRight .img:eq(1) img');
    var oSlideImg = $('#slideBar .m .tab-text img');

    var utm_source = getUrlParameter('utm_source');
    var utm_medium = getUrlParameter('utm_medium');
    var utm_campaign = getUrlParameter('utm_campaign');

    var par = '';

    if (utm_source) {
        par = '?utm_source=' + utm_source;
    }
    if (utm_medium) {
        par += '&utm_medium=' + utm_medium;
    }
    if (utm_campaign) {
        par += '&utm_campaign=' + utm_campaign;
    }
    ZIROOM.ajaxGetSubmit('/site/qrcode' + par, function (data) {
        if (data.code == 200) {
            oImg.attr('src', data.data.normal);
            oSlideImg.attr('src', data.data.normal);
            oImgmin.attr('src', data.data.min);
            $('#fixedAppEwm').attr('src', data.data.dialog);

        }
    });
};

function myTongji() {

}

function sub_feedback(obj) {
    var feedbackHTML = '<div id="feedbackBox2"  class="feedbackBox tc">' +
        '<p class="org mt10">感谢对自如的建议和意见，我们会认真听取</p>' +
        '<p class="org mt10">也请您继续关注我们。</p>' +
        '<p class="org mt10">为您提供优质的服务我们责无旁贷。</p>' +
        '</div>';


    var feedback_value = $("#feedbackTextatea").val();
    if ($.trim(feedback_value).length == 0) {
        layer.msg('请输入内容', {
            time: 800
        })
        return;
    }
    var _this = $(obj);
    _this.val('提交中……').addClass('loading_btn').attr('disabled', 'disabled');
    $.get("/index.php", {
            _p: "ajax",
            _a: "feedback",
            feedback_value: feedback_value
        },
        function (data) {
            _this.val('提 交').removeClass('loading_btn').removeAttr('disabled');
            layer.closeAll();
            layer.open({
                type: 1,
                area: ['350px', 'auto'], //宽高
                title: '意见反馈',
                content: feedbackHTML
            });
            $("#feedbackTextatea").val("");
        });
}



/*set cookie*/
function setCookie(name, value, Days) {
    if (Days == null || Days == '') {
        Days = 300;
    }
    var exp = new Date();
    exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
    //document.cookie = name + "="+ escape (value) + "; path=/;expires=" + exp.toGMTString();  
    document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
}

/*get cookie*/
function getCookie(name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg))
        return unescape(arr[2]);
    else
        return null;
}




$(function () {

    // ZIROOM.ifLogin();
    ZIROOM.changeCity();
    ZIROOM.footer();
    ZIROOM.slideBar();
    // ZIROOM.setQrcode();
})