/**
 * Created by Administrator on 2017/6/15.
 */
//唤醒APP
//判断访问终端
var browser = {
    versions: function () {
        var u = navigator.userAgent,
            app = navigator.appVersion;
        return {
            trident: u.indexOf('Trident') > -1, //IE内核
            presto: u.indexOf('Presto') > -1, //opera内核
            webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
            gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
            mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
            android: u.indexOf('Android') > -1 || u.indexOf('Adr') > -1, //android终端
            iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
            iPad: u.indexOf('iPad') > -1, //是否iPad
            webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
            weixin: u.indexOf('MicroMessenger') > -1, //是否微信 （2015-01-22新增）
            qq: u.match(/QQ\//i) == "QQ/", //是否QQ
            weibo: u.match(/WeiBo/i) == "Weibo" //新浪微博
        };
    }(),
    language: (navigator.browserLanguage || navigator.language).toLowerCase()
}
// 是否微信
function isWeiXin() {
    return browser.versions.weixin;
}
//是微博
function isWeibo() {
    return browser.versions.weibo;
}
// 是QQ
function isQQ() {
    return browser.versions.qq;
}

function isAndroid() {
    return browser.versions.android;
}

function isIos() {
    return browser.versions.ios;
}
var ua = navigator.userAgent.toLowerCase();

function targetToApp(url) {
    if (isIos()) {
        openApp(url);
    } else {
        if (isWeiXin() || isWeibo()) {
            openMask();
        } else {
            openApp(url);
        }
    }
}

function openApp(url) {
    var unEncodeUrl = url;
    var base = new Base64();
    var EncodeUrl = base.encode(unEncodeUrl);
    if (isAndroid()) {
        //android
        //此操作会调起app并阻止接下来的js执行
        $('body').append("<iframe src='ziroom://ziroom.app/openApp?p=" + encodeURIComponent(EncodeUrl) + "' style='display:none' target='' ></iframe>");
        //没有安装应用会执行下面的语句
        setTimeout(function () {
            window.location.href = 'https://static8.ziroom.com/card_clean'
        }, 2000);
    } else {
        //ios
        window.location = 'https://static8.ziroom.com/openApp?p=' + encodeURIComponent(EncodeUrl);
        // console.log('https://static8.ziroom.com/openApp?p='+ encodeURIComponent(EncodeUrl));
    }
}


openMask();

function openMask() {
    debugger
    if ($('body #weixinMaskTip').length < 1) {
        $('body').append('<div id="weixinMaskTip" style="width: 100%; height: 100%; position: fixed; top:0; left:0; background-color: rgba(0, 0, 0, .4); z-index: 999; display: block; overflow: hidden"><img style="width: 40%; position: absolute; right:5%; top:0;" src="http://pic.ziroom.com/zhuanti/2017/kefu/weekApp/weixinMaskTip.png" alt=""></div>');
        $('#weixinMaskTip').bind('click', function () {
            closeMask();
        });
    }
}

function closeMask() {
    $('#weixinMaskTip').unbind('click');
    $('#weixinMaskTip').remove();
}


//2.加密、解密算法封装：
function Base64() {
    // private property
    _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    // public method for encoding
    this.encode = function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;
        input = _utf8_encode(input);
        while (i < input.length) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output + _keyStr.charAt(enc1) + _keyStr.charAt(enc2) + _keyStr.charAt(enc3) + _keyStr.charAt(enc4);
        }
        return output;
    }
    // public method for decoding
    _utf8_encode = function (string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    }
}