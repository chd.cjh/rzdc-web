$(function () {
    $('.slick_detail').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 3000
    });


    $('#appFixed .close').on('click', function () {
        $('#appFixed').hide();
        $.cookie('appFixed', '1', {
            expires: 1,
            path: '/'
        });
    });

    if ($.cookie('appFixed') != '1') {
        $('#appFixed').show();
    } else {
        $('#appFixed').hide();
    }

    function showDel() {
        $('.search .del').show();
    }

    function delDel() {
        $('.search .del').hide();
    }

    var cityCode = $.cookie('CURRENT_CITY_CODE');
    var cityName = 'BJ';
    var cityNameTxt = '北京'
    var curUrl = window.location.href;


    $('#cityShow').html(cityNameTxt);
    document.title = cityNameTxt + '租房|公寓出租_' + cityNameTxt + '租房网站';


    var curCityName = $.cookie('CURRENT_CITY_NAME ');
    var curCityCode = $.cookie('M_CURRENT_CITY_CODE');




    $('#cityBtn').on('click', function () {
        clearKeys();
        $('#cityBox').show();
        $('#main').hide();
    });



    $('#goMain').on('click', function () {
        $('#cityBox').hide();
        $('#main').show();
        toShowList();
    });


    $('#toShowSearchIndex').on('click', function () {
        $.cookie('showListBox', '1', {
            expires: 7,
            path: '/'
        });
        window.location.href = cityName + '/search.html';
    });


    var oMain = $('#main'),
        iWheight = $(window).height(),
        oSlideNav = $('#slideNav'),
        oToMenu = $('#toMenu'),
        oSearchBox = $('#searchBox'),
        oToCancel = $('#toCancel'),
        oToShowSearch = $('#toShowSearch'),
        oSearch = $('#search'),
        sSearchVal = oSearch.val(),
        oPlaceTxt = $('#placeTxt'),
        oRentTxt = $('#rentTxt'),
        oSortTxt = $('#sortTxt'),
        oSubWay = $('#subway'),
        oSearchReset = $('#search_reset'),
        iCancel = 0;



    if ($.cookie('showListBox') == '1') {
        iCancel = 1;
        $.cookie('showListBox', '0', {
            expires: 7,
            path: '/'
        });
        oMain.hide();
        oSearchBox.show();
        oSearch.focus();
    }


    oToCancel.on('click', function () {
        if (iCancel == 1) {
            window.location.href = "/";
        } else {
            oMain.show();
            oSearchBox.hide();
        }

    });

    oToShowSearch.on('click', function () {
        oMain.hide();
        oSearchBox.show();
        oSearch.focus();
    });



    //右侧菜单显示
    oToMenu.on('click', function () {
        oMain.addClass('hidden').css('height', iWheight);
        oSlideNav.show();
    });

    oSlideNav.on('click', function () {
        $(this).hide();
        oMain.removeClass('hidden').css('height', 'auto');
    });


    //列表页
    var oListFilterBox = $('#listFilterBox'),
        aListFilterBoxLi = oListFilterBox.find('li'),
        oHouseList = $('#houseList'),
        oListFilterCon = $('#listFilterCon'),
        aListFilterCon = oListFilterCon.find('.con'),
        iListIndex = 0;



    aListFilterBoxLi.on('click', function () {

        if (oListFilterCon.is(":visible")) {
            hideFilterCon();
        } else {
            iListIndex = $(this).index();
            oListFilterBox.addClass('listFilterBoxFixed');
            oHouseList.addClass('houseListTop90');
            oListFilterCon.find('.con').eq(iListIndex).show().siblings().hide();
            oListFilterCon.show();
            oMain.addClass('hidden').css('height', iWheight);
            $(this).addClass('active').siblings().removeClass('active');
        }




    });

    oListFilterCon.on('click', function () {
        hideFilterCon();
    });

    function hideFilterCon() {
        oListFilterBox.removeClass('listFilterBoxFixed');
        oHouseList.removeClass('houseListTop90');
        oListFilterCon.hide();
        oMain.removeClass('hidden');
        aListFilterBoxLi.removeClass('active');
        oMain.css('height', 'auto');
    }

    aListFilterCon.on('click', function (e) {
        e.stopPropagation();
    });



    //列表页更多筛选
    var oMoreCon = $('#moreCon'),
        aMoreConLi = oMoreCon.find('li'),
        aSpanBtn = oMoreCon.find('.btnsSpan'),
        aSpanBtns = oMoreCon.find('.btnsSpan span'),
        iSpanNum = 0,
        oSpanNum = $('#spanNum'),
        oPlace_1 = $('#place_1'),
        oPlace_3 = $('#place_3'),
        oPlace3Ul = oPlace_3.find('ul'),
        sListType = $.cookie('sListType') ? $.cookie('sListType') : 'subway',
        oToShowMore = $('#toShowMore'),

        oNoneList = $('#noneList'),
        iAjaxFlag = 'list',
        iStep = 0,
        iWtop = 0,
        _html = '',
        sArea = $.cookie('sArea') ? $.cookie('sArea') : '', //区域
        sAreaThree = $.cookie('sAreaThree') ? $.cookie('sAreaThree') : '', //区域三级id
        sAreas = $.cookie('sAreas') ? $.cookie('sAreas') : '', //区域三级
        sArea_line_code = $.cookie('sArea_line_code') ? $.cookie('sArea_line_code') : '', //区域二级名字

        sSubway = $.cookie('sSubway') ? $.cookie('sSubway') : '', //地铁
        sSubways = $.cookie('sSubways') ? $.cookie('sSubways') : '', //地铁三级
        sSubway_line_code = $.cookie('sSubway_line_code') ? $.cookie('sSubway_line_code') : '', //地铁站

        sPrice = $.cookie('sPrice') ? $.cookie('sPrice') : '0', //价格
        sSort = $.cookie('sSort') ? $.cookie('sSort') : '0', //排序
        sKeywords = $.cookie('sKeywords') ? $.cookie('sKeywords') : '',
        is_whole = $.cookie('is_whole') ? $.cookie('is_whole') : ' ', //整租或合租
        room = $.cookie('room') ? $.cookie('room') : ' ', //1居
        is_duanz = $.cookie('is_duanz') ? $.cookie('is_duanz') : '0', //月租
        is_duy = $.cookie('is_duy') ? $.cookie('is_duy') : '0', //独阳
        is_first = $.cookie('is_first') ? $.cookie('is_first') : '0', //首次
        is_wc = $.cookie('is_wc') ? $.cookie('is_wc') : '0'; //独卫

    var urlWhole = getUrlParameter('whole');
    var urlKeyword = getUrlParameter('keyword');

    if (urlWhole) {
        is_whole = urlWhole;
        $.cookie('is_whole', urlWhole, {
            expires: 7,
            path: '/'
        });
    }

    if (urlKeyword) {
        sKeywords = urlKeyword;
        $.cookie('sKeywords', urlKeyword, {
            expires: 7,
            path: '/'
        });
    }


    //首页转到列表，清掉所有搜索条件，详情页回来不清除
    var curUrl = window.location.href;
    if (curUrl.indexOf('/list') > 0 || curUrl.indexOf('/room/') > 0 || curUrl.indexOf('search') > 0) {
        if ($.cookie('curUrl') == 'index') {
            $.cookie('curUrl', 'other', {
                expires: 1,
                path: '/'
            });
            clearKeys();
        }
    } else {
        $.cookie('curUrl', 'index', {
            expires: 1,
            path: '/'
        });
    }


    $('.taglinks li').each(function () {

        if ($(this).text().indexOf('整租') > 0) {
            $(this).attr('href', 'javascript:;');

            $(this).on('click', function () {
                clearKeys();
                $.cookie('curUrl', 'other', {
                    expires: 1,
                    path: '/'
                });
                $.cookie('is_whole', '1', {
                    expires: 7,
                    path: '/'
                });
                window.location.href = cityName + '/search.html';;
            });

        }
        if ($(this).text().indexOf('合租') > 0) {
            $(this).attr('href', 'javascript:;');

            $(this).on('click', function () {
                clearKeys();
                $.cookie('curUrl', 'other', {
                    expires: 1,
                    path: '/'
                });
                $.cookie('is_whole', '0', {
                    expires: 7,
                    path: '/'
                });
                window.location.href = cityName + '/search.html';;
            });

        }
        if ($(this).text().indexOf('月租') > 0) {
            $(this).attr('href', 'javascript:;');

            $(this).on('click', function () {
                clearKeys();
                $.cookie('curUrl', 'other', {
                    expires: 1,
                    path: '/'
                });
                $.cookie('is_duanz', '1', {
                    expires: 7,
                    path: '/'
                });
                window.location.href = cityName + '/search.html';;
            });

        }
    });



    if (sKeywords != '') {
        oToShowSearch.html(sKeywords);
        oSearch.val(sKeywords);
        $('.search .del').show();

    } else {
        $('.search .del').hide();
    }

    if (sListType == 'subway') {
        if (sSubway != '') {
            oSubWay.addClass('active').siblings().removeClass('active');
            chooseSubwayList(sSubway);
            $('#district_list').hide();
            $('#subway_list').show();

            $('#subway_list li[datastr="' + sSubway + '"]').addClass('active').siblings().removeClass('active');
            setTimeout(function () {
                $('#place_3 li[datastr="' + sSubways + '"]').addClass('active').siblings().removeClass('active');
            }, 1000);
            oPlaceTxt.html(sSubways);

            if (sSubways == '0') {
                oPlaceTxt.html(sSubway_line_code);
            } else {
                oPlaceTxt.html(sSubways);
            }



        } else {
            oPlaceTxt.html('位置');
        }
    } else {
        if (sArea != '') {
            $('#district').addClass('active').siblings().removeClass('active');
            chooseDistrictList(sArea);
            $('#district_list').show();
            $('#subway_list').hide();
            $('#district_list li[datastr="' + sArea + '"]').addClass('active').siblings().removeClass('active');
            setTimeout(function () {
                $('#place_3 li[datastr="' + sAreas + '"]').addClass('active').siblings().removeClass('active');
            }, 1000);

            if (sAreaThree == '0') {
                oPlaceTxt.html(sArea_line_code);
            } else {
                oPlaceTxt.html(sAreas);
            }



        } else {
            oPlaceTxt.html('位置');
        }
    }

    if (sAreaThree == '' && sSubways == '') {
        oPlaceTxt.html('位置');
    }




    //价格
    $('#rentCon li').eq(sPrice).addClass('active').siblings().removeClass('active');
    oRentTxt.html($('#rentCon li').eq(sPrice).html());
    if (sPrice == '0') {
        oRentTxt.html('租金');
    }

    //排序
    if (sSort == '0') {
        oSortTxt.html('排序');
        $('#sortCon li').eq(0).addClass('active').siblings().removeClass('active');
    }

    if (sSort == '2') {
        $('#sortCon li').eq(1).addClass('active').siblings().removeClass('active');
        oSortTxt.html($('#sortCon li').eq(1).html());
    }



    //特色多选

    if (is_whole == '1') {
        iSpanNum++;
        $('#wholeList span').eq(0).addClass('active').siblings().removeClass();
    }
    if (is_whole == '0') {
        iSpanNum++;
        $('#wholeList span').eq(1).addClass('active').siblings().removeClass();
    }


    if (room > 0) {
        iSpanNum++;
        $('#roomList span').eq(parseInt(room) - 1).addClass('active').siblings().removeClass();
    }

    if (is_duanz != '0') {
        iSpanNum++;
        $('#tsList span').eq(0).addClass('active');
    }
    if (is_duy != '0') {
        iSpanNum++;
        $('#tsList span').eq(1).addClass('active');
    }
    if (is_first != '0') {
        iSpanNum++;
        $('#tsList span').eq(2).addClass('active');
    }
    if (is_wc != '0') {
        iSpanNum++;
        $('#tsList span').eq(3).addClass('active');
    }

    if (iSpanNum > 0) {
        oSpanNum.html(iSpanNum).show();
    } else {
        oSpanNum.html(iSpanNum).hide();
    }


    toShowList();


    oSubWay.on('click', function () {
        $('#subway_list').show();
        $('#subway_list li').removeClass('active');
        $('#district_list').hide();
        $(this).addClass('active').siblings().removeClass('active');
        sListType = 'subway';
        oPlace_3.hide();
        oPlace3Ul.html('');
        $.cookie('sListType', sListType, {
            expires: 7,
            path: '/'
        });
    });
    $('#district').on('click', function () {
        $('#subway_list').hide();
        $('#district_list').show();
        $('#district_list li').removeClass('active');
        $(this).addClass('active').siblings().removeClass('active');
        sListType = 'district';
        oPlace_3.hide();
        oPlace3Ul.html('');
        $.cookie('sListType', sListType, {
            expires: 7,
            path: '/'
        });
    });




    //地铁列表三级菜单 内容
    $('#subway_list li').on('click', function () {
        oPlace_3.hide();
        oPlace3Ul.html('');

        var _id = $(this).attr('datastr');
        $.cookie('sSubway', _id, {
            expires: 7,
            path: '/'
        });
        $(this).addClass('active').siblings().removeClass('active');

        //        var
        sSubway_line_code = $(this).html();
        $.cookie('sSubway_line_code', sSubway_line_code, {
            expires: 8,
            path: '/'
        });

        if (_id == '0') {

            sSubways = '';
            sAreaThree = '';
            sSubway_line_code = '';
            $.cookie('sSubways', sSubways, {
                expires: 7,
                path: '/'
            });
            $.cookie('sAreaThree', sAreaThree, {
                expires: 7,
                path: '/'
            });
            $.cookie('sSubway_line_code', sSubway_line_code, {
                expires: 7,
                path: '/'
            });
            toShowList();
            hideFilterCon();
            oPlaceTxt.html('位置');
            return;
        }
        chooseSubwayList(_id);
    });

    function chooseSubwayList(_id) {
        var url = '/list/ajax-subway-staion';
        var params = {};


        params.subway_id = _id;
        if (!params.subway_id) {
            params.subway_id = '';
        }

        var json = {
            "data": [
                "西丽",
                "茶光",
                "珠光",
                "龙井",
                "桃源村",
                "安托山",
                "农林",
                "车公庙",
                "上沙",
                "沙尾",
                "石厦",
                "皇岗村",
                "福民",
                "皇岗口岸",
                "赤尾",
                "华强南",
                "华强北",
                "华新",
                "黄木岗",
                "八卦岭",
                "红岭北",
                "笋岗",
                "洪湖",
                "田贝",
                "太安"
            ],
            "info": "ok"
        };

        if (json.info == 'ok') {
            _html = '<li datastr="0">不限</li>';
            for (var i = 0; i < json.data.length; i++) {
                var ss = cityName + 'list';
                _html += '<li datastr="' + json.data[i] + '">' + json.data[i] + '</li>';
            }

            oPlace3Ul.html(_html);
            oPlace_3.show();
        } else {
            alert('获取地铁信息失败');
        }
    }

    //区域列表
    $('#district_list li').on('click', function () {
        oPlace_3.hide();
        oPlace3Ul.html('');

        var _id = $(this).attr('datastr');
        sArea = _id;

        $.cookie('sArea', _id, {
            expires: 7,
            path: '/'
        });
        $(this).addClass('active').siblings().removeClass('active');

        sArea_line_code = $(this).html();
        $.cookie('sArea_line_code', sArea_line_code, {
            expires: 7,
            path: '/'
        });


        if (_id == '0') {
            sAreas = '';
            sAreaThree = ''
            $.cookie('sAreas', sAreas, {
                expires: 7,
                path: '/'
            });
            $.cookie('sAreaThree', sAreaThree, {
                expires: 7,
                path: '/'
            });

            toShowList();
            hideFilterCon();
            oPlaceTxt.html('位置');
            return;
        }
        chooseDistrictList(_id);
    });

    function chooseDistrictList(_id) {
        var url = '/list/ajax-biz';
        var params = {};
        params.districtId = _id;
        if (!params.districtId) {
            params.districtId = '';
        }

        var json = {
            "data": [{
                    "id": "612400051",
                    "bizcircle_name": "蛇口"
                },
                {
                    "id": "612400048",
                    "bizcircle_name": "南头"
                },
                {
                    "id": "611100902",
                    "bizcircle_name": "西丽"
                },
                {
                    "id": "611100877",
                    "bizcircle_name": "科技园北"
                },
                {
                    "id": "611100855",
                    "bizcircle_name": "科技园南"
                },
                {
                    "id": "611100857",
                    "bizcircle_name": "前海"
                },
                {
                    "id": "612400044",
                    "bizcircle_name": "后海"
                },
                {
                    "id": "611100879",
                    "bizcircle_name": "世界之窗"
                },
                {
                    "id": "612400047",
                    "bizcircle_name": "南山中心"
                },
                {
                    "id": "612400049",
                    "bizcircle_name": "南油"
                },
                {
                    "id": "612400045",
                    "bizcircle_name": "华侨城"
                },
                {
                    "id": "611100897",
                    "bizcircle_name": "大学城"
                },
                {
                    "id": "611100871",
                    "bizcircle_name": "桂庙"
                },
                {
                    "id": "611100898",
                    "bizcircle_name": "留仙"
                },
                {
                    "id": "611100944",
                    "bizcircle_name": "花园城"
                },
                {
                    "id": "611100864",
                    "bizcircle_name": "太古城"
                },
                {
                    "id": "611100870",
                    "bizcircle_name": "桃源村"
                },
                {
                    "id": "611100943",
                    "bizcircle_name": "海上世界"
                },
                {
                    "id": "611100978",
                    "bizcircle_name": "侨城东"
                }
            ],
            "info": "ok"
        };

        if (json.info == 'ok') {
            _html = '<li dataid="0">不限</li>';

            for (var i = 0; i < json.data.length; i++) {
                var ss = cityName + 'area';
                _html += '<li dataid="' + json.data[i].id + '"  datastr="' + json.data[i].bizcircle_name + '">' + json.data[i].bizcircle_name + '</li>';
            }

            oPlace3Ul.html(_html);
            oPlace_3.show();
        } else {
            alert('获取区域信息失败');
        }
    }

    //租金筛选
    $('#rentCon li').on('click', function () {
        iStep = 0;
        $(this).addClass('active').siblings().removeClass('active');
        oRentTxt.html($(this).html());
        if ($(this).html() == '不限') {
            oRentTxt.html('租金');
        } else {
            oRentTxt.html($(this).html());
        }
        sPrice = $(this).attr('data');
        $.cookie('sPrice', sPrice, {
            expires: 7,
            path: '/'
        });
        toShowList();
        hideFilterCon();
    });

    //排序筛选
    $('#sortCon li').on('click', function () {
        iStep = 0;
        $(this).addClass('active').siblings().removeClass('active');

        if ($(this).html() == '默认排序') {
            oSortTxt.html('默认');
        } else {
            oSortTxt.html($(this).html());
        }
        sSort = $(this).attr('data');
        $.cookie('sSort', sSort, {
            expires: 7,
            path: '/'
        });
        toShowList();
        hideFilterCon();
    });

    //设置列表页
    $('#place_3').on('click', 'li', function () {
        iStep = 0;
        oHouseList.html('');
        $(this).addClass('active').siblings().removeClass('active');

        if (sListType != 'subway') { //当是区域时
            sAreas = $(this).html();
            sAreaThree = $(this).attr('dataid');
            sSubways = '';
            sSubway_line_code = '';
            if ($(this).html() == '不限') {
                oPlaceTxt.html(sArea_line_code);
            } else {
                oPlaceTxt.html($(this).html());
            }
        } else { //当是地铁站时
            sSubways = $(this).attr('datastr');
            sAreas = '';
            sAreaThree = '';

            if ($(this).html() == '不限') {
                oPlaceTxt.html(sSubway_line_code);
            } else {
                oPlaceTxt.html($(this).html());
            }

        }
        sKeywords = '';
        $.cookie('sSubways', sSubways, {
            expires: 7,
            path: '/'
        });
        $.cookie('sAreas', sAreas, {
            expires: 7,
            path: '/'
        });
        $.cookie('sAreaThree', sAreaThree, {
            expires: 7,
            path: '/'
        });
        //$.cookie('sArea',sArea,{ expires: 7, path: '/' });
        $.cookie('sKeywords', sKeywords, {
            expires: 7,
            path: '/'
        });
        $.cookie('sSubway_line_code', sSubway_line_code, {
            expires: 7,
            path: '/'
        });
        oSearch.val('');
        oToShowSearch.html('输入您想住的区域或小区');




        //写入列表
        toShowList();
        hideFilterCon();


    });


    //
    $('#wholeList span').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            is_whole = " ";
        } else {
            $(this).addClass('active').siblings().removeClass('active');
            is_whole = $(this).attr('data-val');
        }
        $.cookie('is_whole', is_whole, {
            expires: 7,
            path: '/'
        });
        setMoreVal();
    });
    $('#roomList span').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            room = " ";
        } else {
            $(this).addClass('active').siblings().removeClass('active');
            room = $(this).attr('data-val');

        }
        $.cookie('room', room, {
            expires: 7,
            path: '/'
        });
        setMoreVal();
    });
    $('#tsList span').on('click', function () {

        var index = $(this).index();

        if ($(this).hasClass('active')) {
            $(this).removeClass('active').attr('data-val', '0');

        } else {
            $(this).addClass('active').attr('data-val', '1');

        }

        if (index == 0) {
            is_duanz = $(this).attr('data-val');
            $.cookie('is_duanz', is_duanz, {
                expires: 7,
                path: '/'
            });
        }
        if (index == 1) {
            is_duy = $(this).attr('data-val');
            $.cookie('is_duy', is_duy, {
                expires: 7,
                path: '/'
            });

        }
        if (index == 2) {
            is_first = $(this).attr('data-val');
            $.cookie('is_first', is_first, {
                expires: 7,
                path: '/'
            });

        }
        if (index == 3) {
            is_wc = $(this).attr('data-val');
            $.cookie('is_wc', is_wc, {
                expires: 7,
                path: '/'
            });
        }

        setMoreVal();
    });



    function setMoreVal() {
        var aDataVal = $('span[data-val]');
        iSpanNum = 0;
        aDataVal.each(function () {
            if ($(this).hasClass('active')) {
                iSpanNum++;
            }
        });

        if (iSpanNum == 0) {
            oSpanNum.html(iSpanNum).hide();
        } else {
            oSpanNum.html(iSpanNum).show();
        }


        is_duanz = aDataVal.eq(6).attr('data-val'); //月租
        is_duy = aDataVal.eq(7).attr('data-val'); //独阳
        is_first = aDataVal.eq(8).attr('data-val'); //首次
        is_wc = aDataVal.eq(9).attr('data-val'); //独卫
    }

    function isnull(val) {
        if (val == '' || val == 0) {
            return true;
        } else {
            return false;
        }
    }

    //重置按扭
    oSearchReset.on('click', function () {
        iStep = 0;
        aSpanBtns.removeClass('active');
        oSpanNum.html('0').hide();
        iSpanNum = 0;

        //hideFilterCon();

        is_whole = ' '; //整租
        room = ' '; //1居

        is_duanz = '0'; //月租
        is_duy = '0'; //独阳
        is_first = '0'; //首次
        is_wc = '0'; //独卫


        $.cookie('is_whole', is_whole, {
            expires: 7,
            path: '/'
        });
        $.cookie('room', room, {
            expires: 7,
            path: '/'
        });

        $.cookie('is_duanz', is_duanz, {
            expires: 7,
            path: '/'
        });
        $.cookie('is_duy', is_duy, {
            expires: 7,
            path: '/'
        });
        $.cookie('is_first', is_first, {
            expires: 7,
            path: '/'
        });
        $.cookie('is_wc', is_wc, {
            expires: 7,
            path: '/'
        });

        $('#tsList span[data-val]').attr('data-val', '0');
    });

    //确定按扭
    $('#search_confirm').on('click', function () {
        toShowList();
        hideFilterCon();
    });



    oToShowMore.on('click', function () {
        iStep += 10;
        toShowList();
    });



    var imgSrcUpload = '../images/upload.jpg';
    var pre = $('#pre').val();

    function toShowList() {

        if (sKeywords != '') {
            sArea = '';
            sSubways = '';
            sSubway = '';
            sAreaThree = '';
            $.cookie('sArea', sArea, {
                expires: 7,
                path: '/'
            });
            $.cookie('sSubway', sSubway, {
                expires: 7,
                path: '/'
            });
            $.cookie('sSubways', sSubways, {
                expires: 7,
                path: '/'
            });
            $.cookie('sKeywords', sKeywords, {
                expires: 7,
                path: '/'
            });
            $.cookie('sAreaThree', sAreaThree, {
                expires: 7,
                path: '/'
            });

            oToShowSearch.html(sKeywords);
            oSearch.val(sKeywords);

        }



        var url = '/list/ajax-get-data';
        var params = {};
        params.district_code = sArea; //区域
        params.subway_station = sSubways; //地铁站

        params.bizcircle_code = sAreaThree; //区域三级
        params.subway_line_code = $.cookie('sSubway_line_code');; //地铁线


        params.recent_money = sPrice; //价格
        params.sort = sSort; //排序
        params.is_whole = is_whole;
        params.room = room;
        params.key_word = sKeywords;
        params.is_whole = is_whole;
        params.is_duy = is_duy;
        params.is_duanz = is_duanz;
        params.is_first = is_first;
        params.is_wc = is_wc;
        params.step = iStep;

        console.log(params, '****params****');

        var json = {
            "info": "ok",
            "data": [{
                    "id": "60871597",
                    "house_id": "60140238",
                    "list_img": "../images/list_img1.JPG",
                    "room_code": "SZZRGY0817147973_01",
                    "index_no": 1,
                    "balcony_exist": "N",
                    "bizcircle_name": "龙岗中心城",
                    "bizcircle_code": "612400030",
                    "district_name": "龙岗区",
                    "toliet_exist": "Y",
                    "build_size": "89",
                    "duanzuFlag": 0,
                    "is_whole": "0",
                    "dispose_bedroom_amount": "4",
                    "area_name": "龙岗战区",
                    "room_status": "dzz",
                    "sell_price": 1590,
                    "usage_area": 11.6,
                    "house_facing": "东",
                    "compartment_face": "南",
                    "room_name": "龙岗区龙岗中心城3号线(龙岗线)龙城广场金地龙城中央二期4居室-南卧",
                    "duanzuSellPrice": 0,
                    "subway_station_code_first": "龙城广场",
                    "walking_distance_dt_first": 480,
                    "subway_line_code_first": "3号线(龙岗线)",
                    "resblock_name": "金地龙城中央二期",
                    "title": "金地龙城中央二期4居室-南卧"
                },
                {
                    "id": "60912463",
                    "house_id": "60146800",
                    "list_img": "../images/list_img2.JPG",
                    "room_code": "SZZRGZ1017154902_01",
                    "index_no": 1,
                    "balcony_exist": "Y",
                    "bizcircle_name": "大运",
                    "bizcircle_code": "611100888",
                    "district_name": "龙岗区",
                    "toliet_exist": "N",
                    "build_size": "77",
                    "duanzuFlag": 0,
                    "is_whole": "1",
                    "dispose_bedroom_amount": "2",
                    "area_name": "龙岗战区",
                    "room_status": "dzz",
                    "sell_price": 4200,
                    "usage_area": 11.1,
                    "house_facing": "南",
                    "compartment_face": "南",
                    "room_name": "龙岗区大运3号线(龙岗线)大运颐安都会中央二期2居室",
                    "duanzuSellPrice": 0,
                    "subway_station_code_first": "大运",
                    "walking_distance_dt_first": 596,
                    "subway_line_code_first": "3号线(龙岗线)",
                    "resblock_name": "颐安都会中央二期",
                    "title": "颐安都会中央二期2居室-南"
                },
                {
                    "id": "60912913",
                    "house_id": "60146869",
                    "list_img": "",
                    "room_code": "SZZRGY0817154975_05",
                    "index_no": 5,
                    "balcony_exist": "Y",
                    "bizcircle_name": "坂田",
                    "bizcircle_code": "612400027",
                    "district_name": "龙岗区",
                    "toliet_exist": "N",
                    "build_size": "105",
                    "duanzuFlag": 0,
                    "is_whole": "0",
                    "dispose_bedroom_amount": "4",
                    "area_name": "龙坂战区",
                    "room_status": "dzz",
                    "sell_price": 1790,
                    "usage_area": 11.6,
                    "house_facing": "东",
                    "compartment_face": "南",
                    "room_name": "龙岗区坂田5号线(环中线)坂田珠江旭景佳园4居室-南卧",
                    "duanzuSellPrice": 0,
                    "subway_station_code_first": "坂田",
                    "walking_distance_dt_first": 509,
                    "subway_line_code_first": "5号线(环中线)",
                    "resblock_name": "珠江旭景佳园",
                    "title": "珠江旭景佳园4居室-南卧"
                },
                {
                    "id": "60895557",
                    "house_id": "60143988",
                    "list_img": "../images/list_img1.JPG",
                    "room_code": "SZZRGY0817152090_01",
                    "index_no": 1,
                    "balcony_exist": "Y",
                    "bizcircle_name": [
                        "布吉",
                        "大芬"
                    ],
                    "bizcircle_code": [
                        "612400028",
                        "1100001244"
                    ],
                    "district_name": "龙岗区",
                    "toliet_exist": "Y",
                    "build_size": "96.11",
                    "duanzuFlag": 0,
                    "is_whole": "0",
                    "dispose_bedroom_amount": "4",
                    "area_name": "龙岗战区",
                    "room_status": "dzz",
                    "sell_price": 2290,
                    "usage_area": 19.3,
                    "house_facing": "东",
                    "compartment_face": "北",
                    "room_name": "龙岗区布吉3号线(龙岗线)大芬桂芳园七期4居室-北卧",
                    "duanzuSellPrice": 0,
                    "subway_station_code_first": "大芬",
                    "walking_distance_dt_first": 495,
                    "subway_line_code_first": "3号线(龙岗线)",
                    "resblock_name": "桂芳园七期",
                    "title": "桂芳园七期4居室-北卧"
                },
                {
                    "id": "60912910",
                    "house_id": "60146869",
                    "list_img": "",
                    "room_code": "SZZRGY0817154975_01",
                    "index_no": 1,
                    "balcony_exist": "N",
                    "bizcircle_name": "坂田",
                    "bizcircle_code": "612400027",
                    "district_name": "龙岗区",
                    "toliet_exist": "Y",
                    "build_size": "105",
                    "duanzuFlag": 0,
                    "is_whole": "0",
                    "dispose_bedroom_amount": "4",
                    "area_name": "龙坂战区",
                    "room_status": "dzz",
                    "sell_price": 2190,
                    "usage_area": 12.3,
                    "house_facing": "东",
                    "compartment_face": "南",
                    "room_name": "龙岗区坂田5号线(环中线)坂田珠江旭景佳园4居室-南卧",
                    "duanzuSellPrice": 0,
                    "subway_station_code_first": "坂田",
                    "walking_distance_dt_first": 509,
                    "subway_line_code_first": "5号线(环中线)",
                    "resblock_name": "珠江旭景佳园",
                    "title": "珠江旭景佳园4居室-南卧"
                },
                {
                    "id": "60871600",
                    "house_id": "60140238",
                    "list_img": "",
                    "room_code": "SZZRGY0817147973_05",
                    "index_no": 5,
                    "balcony_exist": "Y",
                    "bizcircle_name": "龙岗中心城",
                    "bizcircle_code": "612400030",
                    "district_name": "龙岗区",
                    "toliet_exist": "N",
                    "build_size": "89",
                    "duanzuFlag": 0,
                    "is_whole": "0",
                    "dispose_bedroom_amount": "4",
                    "area_name": "龙岗战区",
                    "room_status": "dzz",
                    "sell_price": 1330,
                    "usage_area": 11.6,
                    "house_facing": "东",
                    "compartment_face": "南",
                    "room_name": "龙岗区龙岗中心城3号线(龙岗线)龙城广场金地龙城中央二期4居室-南卧",
                    "duanzuSellPrice": 0,
                    "subway_station_code_first": "龙城广场",
                    "walking_distance_dt_first": 480,
                    "subway_line_code_first": "3号线(龙岗线)",
                    "resblock_name": "金地龙城中央二期",
                    "title": "金地龙城中央二期4居室-南卧"
                },
                {
                    "id": "60712387",
                    "house_id": "60114804",
                    "list_img": "../images/list_img2.JPG",
                    "room_code": "SZZRGY0817119172_06",
                    "index_no": 6,
                    "balcony_exist": "Y",
                    "bizcircle_name": "坂田",
                    "bizcircle_code": "612400027",
                    "district_name": "龙岗区",
                    "toliet_exist": "N",
                    "build_size": "117",
                    "duanzuFlag": 3,
                    "is_whole": "0",
                    "dispose_bedroom_amount": "5",
                    "area_name": "坂田南大区",
                    "room_status": "dzz",
                    "sell_price": 2370,
                    "usage_area": 13.1,
                    "house_facing": "东",
                    "compartment_face": "西",
                    "room_name": "龙岗区坂田5号线(环中线)五和万科四季花城一期5居室-西卧",
                    "duanzuSellPrice": 1890,
                    "subway_station_code_first": "五和",
                    "walking_distance_dt_first": 231,
                    "subway_line_code_first": "5号线(环中线)",
                    "resblock_name": "万科四季花城一期",
                    "title": "万科四季花城一期5居室-西卧"
                },
                {
                    "id": "60895560",
                    "house_id": "60143988",
                    "list_img": "",
                    "room_code": "SZZRGY0817152090_05",
                    "index_no": 5,
                    "balcony_exist": "Y",
                    "bizcircle_name": [
                        "布吉",
                        "大芬"
                    ],
                    "bizcircle_code": [
                        "612400028",
                        "1100001244"
                    ],
                    "district_name": "龙岗区",
                    "toliet_exist": "N",
                    "build_size": "96.11",
                    "duanzuFlag": 0,
                    "is_whole": "0",
                    "dispose_bedroom_amount": "4",
                    "area_name": "龙岗战区",
                    "room_status": "dzz",
                    "sell_price": 1790,
                    "usage_area": 11.8,
                    "house_facing": "东",
                    "compartment_face": "北",
                    "room_name": "龙岗区布吉3号线(龙岗线)大芬桂芳园七期4居室-北卧",
                    "duanzuSellPrice": 0,
                    "subway_station_code_first": "大芬",
                    "walking_distance_dt_first": 495,
                    "subway_line_code_first": "3号线(龙岗线)",
                    "resblock_name": "桂芳园七期",
                    "title": "桂芳园七期4居室-北卧"
                },
                {
                    "id": "60904936",
                    "house_id": "60144832",
                    "list_img": "",
                    "room_code": "SZZRGZ1117153681_01",
                    "index_no": 1,
                    "balcony_exist": "N",
                    "bizcircle_name": [
                        "车公庙",
                        "竹子林"
                    ],
                    "bizcircle_code": [
                        "612400013",
                        "611100979"
                    ],
                    "district_name": "福田区",
                    "toliet_exist": "N",
                    "build_size": "83",
                    "duanzuFlag": 0,
                    "is_whole": "1",
                    "dispose_bedroom_amount": "2",
                    "area_name": "华西战区",
                    "room_status": "dzz",
                    "sell_price": 7890,
                    "usage_area": 11,
                    "house_facing": "南",
                    "compartment_face": "北",
                    "room_name": "福田区车公庙1号线(罗宝线)竹子林建业大厦2居室",
                    "duanzuSellPrice": 0,
                    "subway_station_code_first": "竹子林",
                    "walking_distance_dt_first": 365,
                    "subway_line_code_first": "1号线(罗宝线)",
                    "resblock_name": "建业大厦",
                    "title": "建业大厦2居室-南"
                },
                {
                    "id": "60815495",
                    "house_id": "60131346",
                    "list_img": "",
                    "room_code": "SZZRGY0817136747_01",
                    "index_no": 1,
                    "balcony_exist": "Y",
                    "bizcircle_name": null,
                    "bizcircle_code": null,
                    "district_name": "龙岗区",
                    "toliet_exist": "N",
                    "build_size": "80",
                    "duanzuFlag": 0,
                    "is_whole": "0",
                    "dispose_bedroom_amount": "4",
                    "area_name": "坂田南大区",
                    "room_status": "dzz",
                    "sell_price": 1790,
                    "usage_area": 8.1,
                    "house_facing": "东",
                    "compartment_face": "南",
                    "room_name": "龙岗区5号线(环中线)五和龙岸花园一期4居室-南卧",
                    "duanzuSellPrice": 0,
                    "subway_station_code_first": "五和",
                    "walking_distance_dt_first": 1281,
                    "subway_line_code_first": "5号线(环中线)",
                    "resblock_name": "龙岸花园一期",
                    "title": "龙岸花园一期4居室-南卧"
                }
            ]
        };

        if (json.info == 'ok') {
            _html = '';
            var oData = json.data;

            oNoneList.hide();

            if (json.data.info == '数据加载完毕') {

                if (iStep == 0) {
                    oToShowMore.hide();
                    oNoneList.show();
                    oHouseList.html('');
                } else {
                    oToShowMore.hide();
                }
                return;

            }

            for (var i = 0; i < oData.length; i++) {

                if (oData[i].room_status == 'tzpzz' || oData[i].room_status == 'zxpzz') {
                    imgSrcUpload = "'../images/pzz.jpg'";
                } else {
                    imgSrcUpload = "'../images/upload.jpg'";
                }

                _html += '<li><a href="###" class="clearfix">' +
                    '<div class="img">';

                if (oData[i].room_status == 'tzpzz' || oData[i].room_status == 'zxpzz') {
                    _html += '<img src="../images/pzz.jpg">';
                } else {
                    _html += '<img src="' + oData[i].list_img + '" onerror="this.src=' + imgSrcUpload + '">';
                }

                _html += '</div>' +
                    '<div class="txt">' +
                    '<h2>' + oData[i].title;

                if (oData[i].balcony_exist == 'Y') {
                    _html += '<span class="y i">阳</span>';
                }
                if (oData[i].toliet_exist == 'Y') {
                    _html += '<span class="w i">卫</span>';
                }
                if (oData[i].compartment_face == '南') {
                    _html += '<span class="n i">南</span>';
                }
                _html += '</h2>';


                if (oData[i].is_whole == '1') {
                    _html += '<p class="ui-gray stit">' + oData[i].build_size + '㎡' +
                        '<span class="z i">整</span>';
                }

                if (oData[i].is_whole == '0') {
                    _html += '<p class="ui-gray stit">' + oData[i].usage_area + '㎡' +
                        '<span class="z i">合</span>';
                }

                if (oData[i].duanzuFlag == 1) {
                    _html += '<span class="h i">月</span>';
                }



                _html += '</p>';

                if (oData[i].subway_line_code_first) {
                    _html += '<p class="ui-gray icon icon_place ellipsis">距' +
                        oData[i].subway_line_code_first +
                        oData[i].subway_station_code_first +
                        '步行' + oData[i].walking_distance_dt_first + '米</p>';
                }

                if (oData[i].duanzuFlag == 3) {
                    _html += '<p class="price">¥' + oData[i].duanzuSellPrice + '</p>'
                } else {
                    _html += '<p class="price">¥' + oData[i].sell_price + '</p>'
                }
                _html += '</div>' +
                    '</a></li>';

            }

            if (oData.length >= 10) {
                oToShowMore.css({
                    'display': 'block'
                });
            }

            if (iStep == 0) {
                oHouseList.html(_html);
        
            } else {
                oHouseList.append(_html);
            }

        } else {
            oToShowMore.hide();
            oNoneList.show();
            oHouseList.html('');
        }
    };


    $('#clearKey').on('click', function () {
        clearKeys();
        toShowList();
    });

    function clearKeys() {
        iStep = 0;
        sArea = ''; //区域
        sAreas = '';
        sSubway = ''; //地铁
        sSubways = ''; //地铁
        sPrice = '0'; //价格
        sSort = '0'; //排序
        is_whole = ' '; //整租或合租
        room = ' '; //1居
        is_duanz = '0'; //月租
        is_duy = '0'; //独阳
        is_first = '0'; //首次
        is_wc = '0'; //独卫
        sKeywords = '';
        sAreaThree = '';
        sArea_line_code = '';
        sSubway_line_code = '';

        $.cookie('sArea', sArea, {
            expires: 7,
            path: '/'
        });
        $.cookie('sAreas', sAreas, {
            expires: 7,
            path: '/'
        });
        $.cookie('sAreaThree', sAreaThree, {
            expires: 7,
            path: '/'
        });
        $.cookie('sSubway', sSubway, {
            expires: 7,
            path: '/'
        });
        $.cookie('sSubways', sSubways, {
            expires: 7,
            path: '/'
        });
        $.cookie('sPrice', sPrice, {
            expires: 7,
            path: '/'
        });
        $.cookie('sSort', sSort, {
            expires: 7,
            path: '/'
        });

        $.cookie('is_whole', is_whole, {
            expires: 7,
            path: '/'
        });
        $.cookie('room', room, {
            expires: 7,
            path: '/'
        });
        $.cookie('is_duanz', is_duanz, {
            expires: 7,
            path: '/'
        });
        $.cookie('is_duy', is_duy, {
            expires: 7,
            path: '/'
        });
        $.cookie('is_first', is_first, {
            expires: 7,
            path: '/'
        });
        $.cookie('is_wc', is_wc, {
            expires: 7,
            path: '/'
        });
        $.cookie('sKeywords', sKeywords, {
            expires: 7,
            path: '/'
        });

        $.cookie('sArea_line_code', sArea_line_code, {
            expires: 7,
            path: '/'
        });
        $.cookie('sSubway_line_code', sSubway_line_code, {
            expires: 7,
            path: '/'
        });


        oPlaceTxt.html('位置');
        oRentTxt.html('租金');
        oSortTxt.html('排序');

        oSearchReset.trigger('click');
        oHouseList.html('');
        oSearch.val('');
        oToShowSearch.html('输入您想住的区域或小区');
        oSubWay.trigger('click');

    }





    //搜索框提交

    $('#submitKeyForm').on('submit', function () {

        sSearchVal = oSearch.val();
        oHouseList.html('');
        sKeywords = sSearchVal;
        $.cookie('sKeywords', sKeywords, {
            expires: 7,
            path: '/'
        });
        oToShowSearch.html(sKeywords);
        oPlaceTxt.html('位置');
        $('#subway').trigger('click');
        if (sSearchVal == '') {
            oToShowSearch.html('输入您想住的区域或小区');
            $('.search .del').show();
        } else {
            $('.search .del').hide();
        }
        toShowList();
        iCancel = 0;
        oToCancel.trigger('click');

        return false;
    });
    $('.search input').on('keyup', function () {
        if ($(this).val() != '') {
            $('.search .del').show();
        } else {
            $('.search .del').hide();
        }
    });

    $('.search .del').on('click', function () {
        $(this).prev('input').val('');
        $(this).hide();
        sKeywords = '';
        $.cookie('sKeywords', sKeywords, {
            expires: 7,
            path: '/'
        });
        oToShowSearch.html('输入您想住的区域或小区');
    });

    //搜索历史列表的事件
    $('#historySearch').on('click', 'a', function () {

        iStep = 0;
        clearKeys();
        sKeywords = $(this).html();
        $.cookie('sKeywords', sKeywords, {
            expires: 7,
            path: '/'
        });
        toShowList();
        iCancel = 0;
        $('.search .del').show();
        oToCancel.trigger('click');
    });

    //搜索历史列表的事件
    $('#hotSearch').on('click', 'a', function () {
        iStep = 0;
        clearKeys();
        sKeywords = $(this).html();
        $.cookie('sKeywords', sKeywords, {
            expires: 7,
            path: '/'
        });
        toShowList();
        iCancel = 0;
        $('.search .del').show();
        oToCancel.trigger('click');
    });

    //清空历史记录
    $('#clearHistory').on('click', function () {
        var url = '/list/ajax-remove-search-history';
        var params = {};
        $.post(url, params, function (json) {
            if (json.info == 'ok') {
                $('#historySearch a').remove();
                oToCancel.trigger('click');
                $('#historySearchCon').hide();
            }
        }, 'json');
        $('.search .del').hide();;
    });

    //详情页

    $('.ui-dialog-ft .close').on('click', function () {
        $(this).parents('.ui-dialog').removeClass('show');
    })

    $('.showMore').on('click', function () {
        if ($(this).html() == '点击展开全部') {
            $(this).html('收起');
            $(this).prev().css('height', 'auto');
        } else {
            $(this).html('点击展开全部');
            $(this).prev().css('height', '120px');
        }

    });


    $('.ui-dialog').on('click', function () {
        $(this).removeClass('show');
    });

    $('.ui-dialog-cnt').on('click', function (e) {
        e.stopPropagation();
    });

});


function getUrlParameter(strParame) {
    var args = new Object();
    var query = location.search.substring(1);

    var pairs = query.split("&");
    for (var i = 0; i < pairs.length; i++) {
        var pos = pairs[i].indexOf('=');
        if (pos == -1) continue;
        var argname = pairs[i].substring(0, pos);
        var value = pairs[i].substring(pos + 1);
        value = decodeURIComponent(value);
        args[argname] = value;
    }
    return args[strParame];
}
